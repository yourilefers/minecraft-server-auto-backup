﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace Minecraft_Server_Auto_Backup
{
    partial class AboutBox1 : Form
    {

        // Initialze mail variables
        public bool Error = false;
        public string Contact_Mail;
        public string Contact_Name;
        public string Contact_Message;
        public string Contact_Type;
        public string Contact_Exception;

        public AboutBox1()
        {
            // Initialize components of the GUI
            InitializeComponent();

            // Set window sizes
            this.Width                  = 433;
            Panel_Contact.Left          = 85;

            // Set label properties
            Label_Programname.Text      = "Program name: "  + Properties.Resources.Program_Name;
            Label_Version.Text          = "Version: "       + Properties.Resources.Version_Main;
            Label_Programauthor.Text    = "Author: "        + Properties.Resources.Program_Author;
            Label_Programcopyright.Text = "Copyright: "     + Properties.Resources.Program_Copyright;
            Contact_Combo_Type.Text     = "Contact";
        }

        private void okButton_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button_Homepage_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.automaticbackup.nl/index.php");
        }

        private void Button_mywebsite_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://home.yourilefers.nl");
        }

        private void Button_Contact_Click(object sender, EventArgs e)
        {
            Panel_Contact.Visible           = true;

            Button_Contact.Enabled          = false;
            Button_Homepage.Enabled         = false;
            Button_mywebsite.Enabled        = false;
            okButton.Enabled                = false;

            Contact_Textbox_Name.Text       = Properties.Settings.Default.Contact_Name;
            Contact_Textbox_Mail.Text       = Properties.Settings.Default.Contact_Mail;
        }

        private void Contact_Button_Back_Click(object sender, EventArgs e)
        {
            Panel_Contact.Visible           = false;

            Button_Contact.Enabled          = true;
            Button_Homepage.Enabled         = true;
            Button_mywebsite.Enabled        = true;
            okButton.Enabled                = true;

            Contact_Textbox_Message.Text    = "";

            Properties.Settings.Default.Contact_Name = Contact_Textbox_Name.Text;
            Properties.Settings.Default.Contact_Mail = Contact_Textbox_Mail.Text;
        }

        private void Contact_Button_Submit_Click(object sender, EventArgs e)
        {
            // GUI changes
            Panel_Sendmail.Visible          = true;
            Contact_Button_Back.Enabled     = false;
            Contact_Button_Submit.Enabled   = false;
            Contact_Combo_Type.Enabled      = false;

            // Check textboxes
            if (Contact_Textbox_Name.Text == "" | Contact_Textbox_Mail.Text == "" | Contact_Textbox_Message.Text == "") {
                
                // Show message about cancellation
                MessageBox.Show("You forgot a textbox. Please fill in all details!");

                // GUI changes
                Panel_Sendmail.Visible          = false;
                Contact_Button_Back.Enabled     = true;
                Contact_Button_Submit.Enabled   = true;
                Contact_Combo_Type.Enabled      = true;

                // Cancel operation
                return;
            }

            // Set strings
            Contact_Name        = Contact_Textbox_Name.Text;
            Contact_Mail        = Contact_Textbox_Mail.Text;
            Contact_Message     = Contact_Textbox_Message.Text;
            Contact_Type        = Contact_Combo_Type.Text;

            // Send the mail
            BGworker_Sendmail.RunWorkerAsync();
        }

        private void BGworker_Sendmail_DoWork(object sender, DoWorkEventArgs e)
        {
            // Initialize mail
            System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage();

            // Set-up mail
            Message.To.Add("lefersyouri@gmail.com");
            Message.From        = new System.Net.Mail.MailAddress(Contact_Mail);
            Message.Subject     = "MSAB - " + Contact_Type + ", from: " + Contact_Name;
            Message.Body        = "Message information\r\nName: " + Contact_Name + "\r\nMail adress: " + Contact_Mail + "\r\nContact type: " + Contact_Type + "\r\nDate: " + DateTime.Now + "\r\n\r\nMessage:\r\n'" + Contact_Message + "'\r\n\r\nEnd of the contact mail.";

            // Initialize SMTP client
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

            // Set-up smtp client
            smtp.UseDefaultCredentials      = false;
            smtp.Credentials                = new System.Net.NetworkCredential("lefersyouri@gmail.com", "fctwente65");
            smtp.EnableSsl                  = true;
            smtp.Port                       = 587;
            smtp.Host                       = "smtp.gmail.com";

            // Send the message
            try
            {

                smtp.Send(Message);
            }
            catch (Exception f)
            {

                Error = true;
                Contact_Exception = f.Message;
            }
        }

        private void BGworker_Sendmail_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

                // Error occured
            if (Error == true) {

                MessageBox.Show("An error occured while sending the message. Please try again later. Error message: " + Contact_Exception);
            }
                // No error occured
            else if (Error == false) {

                // Show message
                MessageBox.Show("The message has been sent succesfully. Thank you for posting this!");

                // Empty message-textbox
                Contact_Textbox_Message.Text = "";
                Contact_Combo_Type.Text      = "Contact";

                // Save mail and name
                Properties.Settings.Default.Contact_Name = Contact_Textbox_Name.Text;
                Properties.Settings.Default.Contact_Mail = Contact_Textbox_Mail.Text;
            }

            // GUI changes
            Panel_Sendmail.Visible          = false;
            Contact_Button_Back.Enabled     = true;
            Contact_Button_Submit.Enabled   = true;
            Contact_Combo_Type.Enabled      = true;
        }

        private void Contact_Combo_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            Contact_Label_Title.Text = Contact_Combo_Type.Text;
        }
    }
}
