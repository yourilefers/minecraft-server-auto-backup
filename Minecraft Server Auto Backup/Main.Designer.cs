﻿namespace Minecraft_Server_Auto_Backup
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label_Copyright = new System.Windows.Forms.Label();
            this.Label_Title = new System.Windows.Forms.Label();
            this.Panel_Main = new System.Windows.Forms.Panel();
            this.Main_Textbox_Target = new System.Windows.Forms.RichTextBox();
            this.Main_Textbox_Source = new System.Windows.Forms.RichTextBox();
            this.Main_Textbox_Version = new System.Windows.Forms.Label();
            this.Main_Image_Loading = new System.Windows.Forms.PictureBox();
            this.Main_Button_About = new System.Windows.Forms.Button();
            this.Main_Button_Start = new System.Windows.Forms.Button();
            this.Main_Button_Settings = new System.Windows.Forms.Button();
            this.Main_Button_Update = new System.Windows.Forms.Button();
            this.Main_Label_Version = new System.Windows.Forms.Label();
            this.Main_Button_Target = new System.Windows.Forms.Button();
            this.Main_Button_Source = new System.Windows.Forms.Button();
            this.Main_Label_Target = new System.Windows.Forms.Label();
            this.Main_Label_Source = new System.Windows.Forms.Label();
            this.Update_Check = new System.ComponentModel.BackgroundWorker();
            this.Panel_Settings = new System.Windows.Forms.Panel();
            this.Settings_Check_Autobackup = new System.Windows.Forms.CheckBox();
            this.Settings_Check_Autoupdate = new System.Windows.Forms.CheckBox();
            this.Panel_Time_Option2 = new System.Windows.Forms.Panel();
            this.Settings_Check_Everyday = new System.Windows.Forms.CheckBox();
            this.Settings_Combo_Time = new System.Windows.Forms.ComboBox();
            this.Settings_Combo_Day = new System.Windows.Forms.ComboBox();
            this.Settings_Radio_Option2 = new System.Windows.Forms.RadioButton();
            this.Settings_Radio_Option1 = new System.Windows.Forms.RadioButton();
            this.Panel_Time_Option1 = new System.Windows.Forms.Panel();
            this.Settings_Textbox_Time = new System.Windows.Forms.TextBox();
            this.Settings_Combo_Selection = new System.Windows.Forms.ComboBox();
            this.Settings_Label_Time = new System.Windows.Forms.Label();
            this.Settings_Button_Back = new System.Windows.Forms.Button();
            this.Settings_Label_Title = new System.Windows.Forms.Label();
            this.Panel_Running = new System.Windows.Forms.Panel();
            this.Running_Label_Info = new System.Windows.Forms.Label();
            this.Running_Button_Now = new System.Windows.Forms.Button();
            this.Running_Button_Stop = new System.Windows.Forms.Button();
            this.Running_Label_Title = new System.Windows.Forms.Label();
            this.Running_Textbox_Log = new System.Windows.Forms.RichTextBox();
            this.Settings_Button_Reset = new System.Windows.Forms.Button();
            this.Panel_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Main_Image_Loading)).BeginInit();
            this.Panel_Settings.SuspendLayout();
            this.Panel_Time_Option2.SuspendLayout();
            this.Panel_Time_Option1.SuspendLayout();
            this.Panel_Running.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label_Copyright
            // 
            this.Label_Copyright.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Copyright.AutoSize = true;
            this.Label_Copyright.BackColor = System.Drawing.Color.Transparent;
            this.Label_Copyright.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Label_Copyright.Location = new System.Drawing.Point(111, 449);
            this.Label_Copyright.Name = "Label_Copyright";
            this.Label_Copyright.Size = new System.Drawing.Size(158, 13);
            this.Label_Copyright.TabIndex = 0;
            this.Label_Copyright.Text = "Copyright (c) 2012 - Youri Lefers";
            // 
            // Label_Title
            // 
            this.Label_Title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Title.AutoSize = true;
            this.Label_Title.BackColor = System.Drawing.Color.Transparent;
            this.Label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Title.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Label_Title.Location = new System.Drawing.Point(88, 9);
            this.Label_Title.Name = "Label_Title";
            this.Label_Title.Size = new System.Drawing.Size(204, 29);
            this.Label_Title.TabIndex = 2;
            this.Label_Title.Text = "Automatic Backup";
            // 
            // Panel_Main
            // 
            this.Panel_Main.BackColor = System.Drawing.Color.Transparent;
            this.Panel_Main.Controls.Add(this.Main_Textbox_Target);
            this.Panel_Main.Controls.Add(this.Main_Textbox_Source);
            this.Panel_Main.Controls.Add(this.Main_Textbox_Version);
            this.Panel_Main.Controls.Add(this.Main_Image_Loading);
            this.Panel_Main.Controls.Add(this.Main_Button_About);
            this.Panel_Main.Controls.Add(this.Main_Button_Start);
            this.Panel_Main.Controls.Add(this.Main_Button_Settings);
            this.Panel_Main.Controls.Add(this.Main_Button_Update);
            this.Panel_Main.Controls.Add(this.Main_Label_Version);
            this.Panel_Main.Controls.Add(this.Main_Button_Target);
            this.Panel_Main.Controls.Add(this.Main_Button_Source);
            this.Panel_Main.Controls.Add(this.Main_Label_Target);
            this.Panel_Main.Controls.Add(this.Main_Label_Source);
            this.Panel_Main.Location = new System.Drawing.Point(12, 41);
            this.Panel_Main.Name = "Panel_Main";
            this.Panel_Main.Size = new System.Drawing.Size(356, 405);
            this.Panel_Main.TabIndex = 3;
            // 
            // Main_Textbox_Target
            // 
            this.Main_Textbox_Target.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Main_Textbox_Target.DetectUrls = false;
            this.Main_Textbox_Target.Location = new System.Drawing.Point(60, 98);
            this.Main_Textbox_Target.Name = "Main_Textbox_Target";
            this.Main_Textbox_Target.ReadOnly = true;
            this.Main_Textbox_Target.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.Main_Textbox_Target.Size = new System.Drawing.Size(240, 35);
            this.Main_Textbox_Target.TabIndex = 16;
            this.Main_Textbox_Target.Text = "";
            this.Main_Textbox_Target.TextChanged += new System.EventHandler(this.Main_Textbox_Target_TextChanged);
            // 
            // Main_Textbox_Source
            // 
            this.Main_Textbox_Source.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Main_Textbox_Source.DetectUrls = false;
            this.Main_Textbox_Source.Location = new System.Drawing.Point(60, 38);
            this.Main_Textbox_Source.Name = "Main_Textbox_Source";
            this.Main_Textbox_Source.ReadOnly = true;
            this.Main_Textbox_Source.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.Main_Textbox_Source.Size = new System.Drawing.Size(240, 35);
            this.Main_Textbox_Source.TabIndex = 15;
            this.Main_Textbox_Source.Text = "";
            this.Main_Textbox_Source.TextChanged += new System.EventHandler(this.Main_Textbox_Source_TextChanged);
            // 
            // Main_Textbox_Version
            // 
            this.Main_Textbox_Version.AutoSize = true;
            this.Main_Textbox_Version.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Main_Textbox_Version.Location = new System.Drawing.Point(57, 195);
            this.Main_Textbox_Version.Name = "Main_Textbox_Version";
            this.Main_Textbox_Version.Size = new System.Drawing.Size(76, 26);
            this.Main_Textbox_Version.TabIndex = 14;
            this.Main_Textbox_Version.Text = "Your version:\r\nLatest version:";
            this.Main_Textbox_Version.DoubleClick += new System.EventHandler(this.Main_Textbox_Version_DoubleClick);
            // 
            // Main_Image_Loading
            // 
            this.Main_Image_Loading.Image = global::Minecraft_Server_Auto_Backup.Properties.Resources.ajax_loader;
            this.Main_Image_Loading.Location = new System.Drawing.Point(69, 233);
            this.Main_Image_Loading.Name = "Main_Image_Loading";
            this.Main_Image_Loading.Size = new System.Drawing.Size(219, 20);
            this.Main_Image_Loading.TabIndex = 13;
            this.Main_Image_Loading.TabStop = false;
            this.Main_Image_Loading.Visible = false;
            // 
            // Main_Button_About
            // 
            this.Main_Button_About.Location = new System.Drawing.Point(58, 319);
            this.Main_Button_About.Name = "Main_Button_About";
            this.Main_Button_About.Size = new System.Drawing.Size(119, 36);
            this.Main_Button_About.TabIndex = 12;
            this.Main_Button_About.Text = "About / Bug report / Contact / Help me";
            this.Main_Button_About.UseVisualStyleBackColor = true;
            this.Main_Button_About.Click += new System.EventHandler(this.Main_Button_About_Click);
            // 
            // Main_Button_Start
            // 
            this.Main_Button_Start.Location = new System.Drawing.Point(58, 277);
            this.Main_Button_Start.Name = "Main_Button_Start";
            this.Main_Button_Start.Size = new System.Drawing.Size(240, 36);
            this.Main_Button_Start.TabIndex = 10;
            this.Main_Button_Start.Text = "Start";
            this.Main_Button_Start.UseVisualStyleBackColor = true;
            this.Main_Button_Start.Click += new System.EventHandler(this.Main_Button_Start_Click);
            // 
            // Main_Button_Settings
            // 
            this.Main_Button_Settings.Location = new System.Drawing.Point(183, 319);
            this.Main_Button_Settings.Name = "Main_Button_Settings";
            this.Main_Button_Settings.Size = new System.Drawing.Size(115, 36);
            this.Main_Button_Settings.TabIndex = 9;
            this.Main_Button_Settings.Text = "Settings >";
            this.Main_Button_Settings.UseVisualStyleBackColor = true;
            this.Main_Button_Settings.Click += new System.EventHandler(this.Main_Button_Settings_Click);
            // 
            // Main_Button_Update
            // 
            this.Main_Button_Update.Location = new System.Drawing.Point(218, 192);
            this.Main_Button_Update.Name = "Main_Button_Update";
            this.Main_Button_Update.Size = new System.Drawing.Size(82, 35);
            this.Main_Button_Update.TabIndex = 8;
            this.Main_Button_Update.Text = "Update\r\ncontrol";
            this.Main_Button_Update.UseVisualStyleBackColor = true;
            this.Main_Button_Update.Click += new System.EventHandler(this.Main_Button_Update_Click);
            // 
            // Main_Label_Version
            // 
            this.Main_Label_Version.AutoSize = true;
            this.Main_Label_Version.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Main_Label_Version.Location = new System.Drawing.Point(57, 180);
            this.Main_Label_Version.Name = "Main_Label_Version";
            this.Main_Label_Version.Size = new System.Drawing.Size(77, 13);
            this.Main_Label_Version.TabIndex = 6;
            this.Main_Label_Version.Text = "Update control";
            this.Main_Label_Version.DoubleClick += new System.EventHandler(this.Main_Label_Version_DoubleClick);
            // 
            // Main_Button_Target
            // 
            this.Main_Button_Target.Location = new System.Drawing.Point(181, 139);
            this.Main_Button_Target.Name = "Main_Button_Target";
            this.Main_Button_Target.Size = new System.Drawing.Size(119, 23);
            this.Main_Button_Target.TabIndex = 5;
            this.Main_Button_Target.Text = "Select destination...";
            this.Main_Button_Target.UseVisualStyleBackColor = true;
            this.Main_Button_Target.Click += new System.EventHandler(this.Main_Button_Target_Click);
            // 
            // Main_Button_Source
            // 
            this.Main_Button_Source.Location = new System.Drawing.Point(60, 139);
            this.Main_Button_Source.Name = "Main_Button_Source";
            this.Main_Button_Source.Size = new System.Drawing.Size(115, 23);
            this.Main_Button_Source.TabIndex = 4;
            this.Main_Button_Source.Text = "Select source...";
            this.Main_Button_Source.UseVisualStyleBackColor = true;
            this.Main_Button_Source.Click += new System.EventHandler(this.Main_Button_Source_Click);
            // 
            // Main_Label_Target
            // 
            this.Main_Label_Target.AutoSize = true;
            this.Main_Label_Target.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Main_Label_Target.Location = new System.Drawing.Point(57, 82);
            this.Main_Label_Target.Name = "Main_Label_Target";
            this.Main_Label_Target.Size = new System.Drawing.Size(134, 13);
            this.Main_Label_Target.TabIndex = 2;
            this.Main_Label_Target.Text = "Select destination directory";
            // 
            // Main_Label_Source
            // 
            this.Main_Label_Source.AutoSize = true;
            this.Main_Label_Source.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Main_Label_Source.Location = new System.Drawing.Point(57, 22);
            this.Main_Label_Source.Name = "Main_Label_Source";
            this.Main_Label_Source.Size = new System.Drawing.Size(115, 13);
            this.Main_Label_Source.TabIndex = 0;
            this.Main_Label_Source.Text = "Select source directory";
            // 
            // Update_Check
            // 
            this.Update_Check.WorkerSupportsCancellation = true;
            this.Update_Check.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Update_Check_DoWork);
            this.Update_Check.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Update_Check_RunWorkerCompleted);
            // 
            // Panel_Settings
            // 
            this.Panel_Settings.BackColor = System.Drawing.Color.Transparent;
            this.Panel_Settings.Controls.Add(this.Settings_Button_Reset);
            this.Panel_Settings.Controls.Add(this.Settings_Check_Autobackup);
            this.Panel_Settings.Controls.Add(this.Settings_Check_Autoupdate);
            this.Panel_Settings.Controls.Add(this.Panel_Time_Option2);
            this.Panel_Settings.Controls.Add(this.Settings_Radio_Option2);
            this.Panel_Settings.Controls.Add(this.Settings_Radio_Option1);
            this.Panel_Settings.Controls.Add(this.Panel_Time_Option1);
            this.Panel_Settings.Controls.Add(this.Settings_Label_Time);
            this.Panel_Settings.Controls.Add(this.Settings_Button_Back);
            this.Panel_Settings.Controls.Add(this.Settings_Label_Title);
            this.Panel_Settings.Location = new System.Drawing.Point(401, 41);
            this.Panel_Settings.Name = "Panel_Settings";
            this.Panel_Settings.Size = new System.Drawing.Size(356, 405);
            this.Panel_Settings.TabIndex = 4;
            this.Panel_Settings.Visible = false;
            // 
            // Settings_Check_Autobackup
            // 
            this.Settings_Check_Autobackup.AutoSize = true;
            this.Settings_Check_Autobackup.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Settings_Check_Autobackup.Location = new System.Drawing.Point(58, 206);
            this.Settings_Check_Autobackup.Name = "Settings_Check_Autobackup";
            this.Settings_Check_Autobackup.Size = new System.Drawing.Size(154, 17);
            this.Settings_Check_Autobackup.TabIndex = 21;
            this.Settings_Check_Autobackup.Text = "Disable automatic backups";
            this.Settings_Check_Autobackup.UseVisualStyleBackColor = true;
            this.Settings_Check_Autobackup.CheckedChanged += new System.EventHandler(this.Settings_Check_Autobackup_CheckedChanged);
            // 
            // Settings_Check_Autoupdate
            // 
            this.Settings_Check_Autoupdate.AutoSize = true;
            this.Settings_Check_Autoupdate.Checked = true;
            this.Settings_Check_Autoupdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Settings_Check_Autoupdate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Settings_Check_Autoupdate.Location = new System.Drawing.Point(58, 229);
            this.Settings_Check_Autoupdate.Name = "Settings_Check_Autoupdate";
            this.Settings_Check_Autoupdate.Size = new System.Drawing.Size(177, 17);
            this.Settings_Check_Autoupdate.TabIndex = 20;
            this.Settings_Check_Autoupdate.Text = "Automatically check for updates";
            this.Settings_Check_Autoupdate.UseVisualStyleBackColor = true;
            this.Settings_Check_Autoupdate.CheckedChanged += new System.EventHandler(this.Settings_Check_Autoupdate_CheckedChanged);
            // 
            // Panel_Time_Option2
            // 
            this.Panel_Time_Option2.Controls.Add(this.Settings_Check_Everyday);
            this.Panel_Time_Option2.Controls.Add(this.Settings_Combo_Time);
            this.Panel_Time_Option2.Controls.Add(this.Settings_Combo_Day);
            this.Panel_Time_Option2.Enabled = false;
            this.Panel_Time_Option2.Location = new System.Drawing.Point(60, 147);
            this.Panel_Time_Option2.Name = "Panel_Time_Option2";
            this.Panel_Time_Option2.Size = new System.Drawing.Size(240, 53);
            this.Panel_Time_Option2.TabIndex = 19;
            // 
            // Settings_Check_Everyday
            // 
            this.Settings_Check_Everyday.AutoSize = true;
            this.Settings_Check_Everyday.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Settings_Check_Everyday.Location = new System.Drawing.Point(3, 29);
            this.Settings_Check_Everyday.Name = "Settings_Check_Everyday";
            this.Settings_Check_Everyday.Size = new System.Drawing.Size(49, 17);
            this.Settings_Check_Everyday.TabIndex = 3;
            this.Settings_Check_Everyday.Text = "Daily";
            this.Settings_Check_Everyday.UseVisualStyleBackColor = true;
            this.Settings_Check_Everyday.CheckedChanged += new System.EventHandler(this.Settings_Check_Everyday_CheckedChanged);
            // 
            // Settings_Combo_Time
            // 
            this.Settings_Combo_Time.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Settings_Combo_Time.FormattingEnabled = true;
            this.Settings_Combo_Time.Items.AddRange(new object[] {
            "00:00",
            "01:00",
            "02:00",
            "03:00",
            "04:00",
            "05:00",
            "06:00",
            "07:00",
            "08:00",
            "09:00",
            "10:00",
            "11:00",
            "12:00",
            "13:00",
            "14:00",
            "15:00",
            "16:00",
            "17:00",
            "18:00",
            "19:00",
            "20:00",
            "21:00",
            "22:00",
            "23:00"});
            this.Settings_Combo_Time.Location = new System.Drawing.Point(164, 3);
            this.Settings_Combo_Time.Name = "Settings_Combo_Time";
            this.Settings_Combo_Time.Size = new System.Drawing.Size(73, 21);
            this.Settings_Combo_Time.TabIndex = 2;
            this.Settings_Combo_Time.SelectedIndexChanged += new System.EventHandler(this.Settings_Combo_Time_SelectedIndexChanged);
            // 
            // Settings_Combo_Day
            // 
            this.Settings_Combo_Day.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Settings_Combo_Day.FormattingEnabled = true;
            this.Settings_Combo_Day.Items.AddRange(new object[] {
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sonday"});
            this.Settings_Combo_Day.Location = new System.Drawing.Point(3, 3);
            this.Settings_Combo_Day.Name = "Settings_Combo_Day";
            this.Settings_Combo_Day.Size = new System.Drawing.Size(155, 21);
            this.Settings_Combo_Day.TabIndex = 1;
            this.Settings_Combo_Day.SelectedIndexChanged += new System.EventHandler(this.Settings_Combo_Day_SelectedIndexChanged);
            // 
            // Settings_Radio_Option2
            // 
            this.Settings_Radio_Option2.AutoSize = true;
            this.Settings_Radio_Option2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Settings_Radio_Option2.Location = new System.Drawing.Point(60, 124);
            this.Settings_Radio_Option2.Name = "Settings_Radio_Option2";
            this.Settings_Radio_Option2.Size = new System.Drawing.Size(108, 17);
            this.Settings_Radio_Option2.TabIndex = 18;
            this.Settings_Radio_Option2.Text = "Option 2 - precise";
            this.Settings_Radio_Option2.UseVisualStyleBackColor = true;
            this.Settings_Radio_Option2.CheckedChanged += new System.EventHandler(this.Settings_Radio_Option2_CheckedChanged);
            // 
            // Settings_Radio_Option1
            // 
            this.Settings_Radio_Option1.AutoSize = true;
            this.Settings_Radio_Option1.Checked = true;
            this.Settings_Radio_Option1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Settings_Radio_Option1.Location = new System.Drawing.Point(60, 67);
            this.Settings_Radio_Option1.Name = "Settings_Radio_Option1";
            this.Settings_Radio_Option1.Size = new System.Drawing.Size(191, 17);
            this.Settings_Radio_Option1.TabIndex = 17;
            this.Settings_Radio_Option1.TabStop = true;
            this.Settings_Radio_Option1.Text = "Option 1 - set time in hours/minutes";
            this.Settings_Radio_Option1.UseVisualStyleBackColor = true;
            this.Settings_Radio_Option1.CheckedChanged += new System.EventHandler(this.Settings_Radio_Option1_CheckedChanged);
            // 
            // Panel_Time_Option1
            // 
            this.Panel_Time_Option1.Controls.Add(this.Settings_Textbox_Time);
            this.Panel_Time_Option1.Controls.Add(this.Settings_Combo_Selection);
            this.Panel_Time_Option1.Location = new System.Drawing.Point(60, 90);
            this.Panel_Time_Option1.Name = "Panel_Time_Option1";
            this.Panel_Time_Option1.Size = new System.Drawing.Size(240, 28);
            this.Panel_Time_Option1.TabIndex = 16;
            // 
            // Settings_Textbox_Time
            // 
            this.Settings_Textbox_Time.Location = new System.Drawing.Point(3, 4);
            this.Settings_Textbox_Time.Name = "Settings_Textbox_Time";
            this.Settings_Textbox_Time.Size = new System.Drawing.Size(107, 20);
            this.Settings_Textbox_Time.TabIndex = 1;
            this.Settings_Textbox_Time.Text = "60";
            this.Settings_Textbox_Time.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Settings_Textbox_Time.TextChanged += new System.EventHandler(this.Settings_Textbox_Time_TextChanged);
            this.Settings_Textbox_Time.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Settings_Textbox_Time_KeyPress);
            // 
            // Settings_Combo_Selection
            // 
            this.Settings_Combo_Selection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Settings_Combo_Selection.FormattingEnabled = true;
            this.Settings_Combo_Selection.Items.AddRange(new object[] {
            "Minutes",
            "Hours"});
            this.Settings_Combo_Selection.Location = new System.Drawing.Point(116, 3);
            this.Settings_Combo_Selection.Name = "Settings_Combo_Selection";
            this.Settings_Combo_Selection.Size = new System.Drawing.Size(121, 21);
            this.Settings_Combo_Selection.TabIndex = 0;
            this.Settings_Combo_Selection.SelectedIndexChanged += new System.EventHandler(this.Settings_Combo_Selection_SelectedIndexChanged);
            // 
            // Settings_Label_Time
            // 
            this.Settings_Label_Time.AutoSize = true;
            this.Settings_Label_Time.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Settings_Label_Time.Location = new System.Drawing.Point(57, 45);
            this.Settings_Label_Time.Name = "Settings_Label_Time";
            this.Settings_Label_Time.Size = new System.Drawing.Size(145, 13);
            this.Settings_Label_Time.TabIndex = 15;
            this.Settings_Label_Time.Text = "Select here your backup time";
            // 
            // Settings_Button_Back
            // 
            this.Settings_Button_Back.Location = new System.Drawing.Point(60, 319);
            this.Settings_Button_Back.Name = "Settings_Button_Back";
            this.Settings_Button_Back.Size = new System.Drawing.Size(115, 36);
            this.Settings_Button_Back.TabIndex = 13;
            this.Settings_Button_Back.Text = "< Back";
            this.Settings_Button_Back.UseVisualStyleBackColor = true;
            this.Settings_Button_Back.Click += new System.EventHandler(this.Settings_Button_Back_Click);
            // 
            // Settings_Label_Title
            // 
            this.Settings_Label_Title.AutoSize = true;
            this.Settings_Label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Settings_Label_Title.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Settings_Label_Title.Location = new System.Drawing.Point(135, 13);
            this.Settings_Label_Title.Name = "Settings_Label_Title";
            this.Settings_Label_Title.Size = new System.Drawing.Size(91, 26);
            this.Settings_Label_Title.TabIndex = 0;
            this.Settings_Label_Title.Text = "Settings";
            // 
            // Panel_Running
            // 
            this.Panel_Running.BackColor = System.Drawing.Color.Transparent;
            this.Panel_Running.Controls.Add(this.Running_Label_Info);
            this.Panel_Running.Controls.Add(this.Running_Button_Now);
            this.Panel_Running.Controls.Add(this.Running_Button_Stop);
            this.Panel_Running.Controls.Add(this.Running_Label_Title);
            this.Panel_Running.Controls.Add(this.Running_Textbox_Log);
            this.Panel_Running.Location = new System.Drawing.Point(763, 41);
            this.Panel_Running.Name = "Panel_Running";
            this.Panel_Running.Size = new System.Drawing.Size(200, 253);
            this.Panel_Running.TabIndex = 5;
            this.Panel_Running.Visible = false;
            // 
            // Running_Label_Info
            // 
            this.Running_Label_Info.AutoSize = true;
            this.Running_Label_Info.Location = new System.Drawing.Point(3, 55);
            this.Running_Label_Info.Name = "Running_Label_Info";
            this.Running_Label_Info.Size = new System.Drawing.Size(151, 39);
            this.Running_Label_Info.TabIndex = 4;
            this.Running_Label_Info.Text = "Next backup in :\r\nBackup number (overall) :\r\nBackup number (this session) :";
            // 
            // Running_Button_Now
            // 
            this.Running_Button_Now.Location = new System.Drawing.Point(122, 29);
            this.Running_Button_Now.Name = "Running_Button_Now";
            this.Running_Button_Now.Size = new System.Drawing.Size(75, 23);
            this.Running_Button_Now.TabIndex = 3;
            this.Running_Button_Now.Text = "Now";
            this.Running_Button_Now.UseVisualStyleBackColor = true;
            this.Running_Button_Now.Click += new System.EventHandler(this.Running_Button_Now_Click);
            // 
            // Running_Button_Stop
            // 
            this.Running_Button_Stop.Location = new System.Drawing.Point(3, 29);
            this.Running_Button_Stop.Name = "Running_Button_Stop";
            this.Running_Button_Stop.Size = new System.Drawing.Size(75, 23);
            this.Running_Button_Stop.TabIndex = 2;
            this.Running_Button_Stop.Text = "Stop";
            this.Running_Button_Stop.UseVisualStyleBackColor = true;
            this.Running_Button_Stop.Click += new System.EventHandler(this.Running_Button_Stop_Click);
            // 
            // Running_Label_Title
            // 
            this.Running_Label_Title.AutoSize = true;
            this.Running_Label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Running_Label_Title.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Running_Label_Title.Location = new System.Drawing.Point(55, 0);
            this.Running_Label_Title.Name = "Running_Label_Title";
            this.Running_Label_Title.Size = new System.Drawing.Size(93, 26);
            this.Running_Label_Title.TabIndex = 1;
            this.Running_Label_Title.Text = "Running";
            // 
            // Running_Textbox_Log
            // 
            this.Running_Textbox_Log.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Running_Textbox_Log.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Running_Textbox_Log.DetectUrls = false;
            this.Running_Textbox_Log.Location = new System.Drawing.Point(3, 98);
            this.Running_Textbox_Log.Name = "Running_Textbox_Log";
            this.Running_Textbox_Log.ReadOnly = true;
            this.Running_Textbox_Log.Size = new System.Drawing.Size(194, 145);
            this.Running_Textbox_Log.TabIndex = 0;
            this.Running_Textbox_Log.Text = "Starting backup service...\nStarting log...";
            // 
            // Settings_Button_Reset
            // 
            this.Settings_Button_Reset.Location = new System.Drawing.Point(181, 319);
            this.Settings_Button_Reset.Name = "Settings_Button_Reset";
            this.Settings_Button_Reset.Size = new System.Drawing.Size(119, 36);
            this.Settings_Button_Reset.TabIndex = 23;
            this.Settings_Button_Reset.Text = "Reset ...";
            this.Settings_Button_Reset.UseVisualStyleBackColor = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GrayText;
            this.ClientSize = new System.Drawing.Size(1098, 471);
            this.Controls.Add(this.Panel_Running);
            this.Controls.Add(this.Panel_Settings);
            this.Controls.Add(this.Panel_Main);
            this.Controls.Add(this.Label_Title);
            this.Controls.Add(this.Label_Copyright);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automatic Backup";
            this.Panel_Main.ResumeLayout(false);
            this.Panel_Main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Main_Image_Loading)).EndInit();
            this.Panel_Settings.ResumeLayout(false);
            this.Panel_Settings.PerformLayout();
            this.Panel_Time_Option2.ResumeLayout(false);
            this.Panel_Time_Option2.PerformLayout();
            this.Panel_Time_Option1.ResumeLayout(false);
            this.Panel_Time_Option1.PerformLayout();
            this.Panel_Running.ResumeLayout(false);
            this.Panel_Running.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label_Copyright;
        private System.Windows.Forms.Label Label_Title;
        private System.Windows.Forms.Panel Panel_Main;
        private System.Windows.Forms.Label Main_Label_Source;
        private System.Windows.Forms.Button Main_Button_Target;
        private System.Windows.Forms.Button Main_Button_Source;
        private System.Windows.Forms.Label Main_Label_Target;
        private System.Windows.Forms.Button Main_Button_Update;
        private System.Windows.Forms.Label Main_Label_Version;
        private System.Windows.Forms.Button Main_Button_Start;
        private System.Windows.Forms.Button Main_Button_Settings;
        private System.ComponentModel.BackgroundWorker Update_Check;
        private System.Windows.Forms.Button Main_Button_About;
        private System.Windows.Forms.PictureBox Main_Image_Loading;
        private System.Windows.Forms.Panel Panel_Settings;
        private System.Windows.Forms.Button Settings_Button_Back;
        private System.Windows.Forms.Label Settings_Label_Title;
        private System.Windows.Forms.Label Settings_Label_Time;
        private System.Windows.Forms.RadioButton Settings_Radio_Option1;
        private System.Windows.Forms.Panel Panel_Time_Option1;
        private System.Windows.Forms.Panel Panel_Time_Option2;
        private System.Windows.Forms.CheckBox Settings_Check_Everyday;
        private System.Windows.Forms.ComboBox Settings_Combo_Time;
        private System.Windows.Forms.ComboBox Settings_Combo_Day;
        private System.Windows.Forms.RadioButton Settings_Radio_Option2;
        private System.Windows.Forms.TextBox Settings_Textbox_Time;
        private System.Windows.Forms.ComboBox Settings_Combo_Selection;
        private System.Windows.Forms.Label Main_Textbox_Version;
        private System.Windows.Forms.RichTextBox Main_Textbox_Source;
        private System.Windows.Forms.RichTextBox Main_Textbox_Target;
        private System.Windows.Forms.CheckBox Settings_Check_Autobackup;
        private System.Windows.Forms.CheckBox Settings_Check_Autoupdate;
        private System.Windows.Forms.Panel Panel_Running;
        private System.Windows.Forms.Button Running_Button_Now;
        private System.Windows.Forms.Button Running_Button_Stop;
        private System.Windows.Forms.Label Running_Label_Title;
        private System.Windows.Forms.RichTextBox Running_Textbox_Log;
        private System.Windows.Forms.Label Running_Label_Info;
        private System.Windows.Forms.Button Settings_Button_Reset;
    }
}