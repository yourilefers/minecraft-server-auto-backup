﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Timers;
using System.Net;
using System.Threading;
using Update;

namespace Minecraft_Server_Auto_Backup
{
    public partial class Main : Form
    {
        #region Public variables
        
        // Main variables
        IniFile Settings                        = new IniFile(Properties.Resources.Settings);
        public string Server_Folder_Path;
        public string targetPath;
        public string sourcePath;
        public bool Auto_Backup_Enabled         = Properties.Settings.Default.Settings_Auto_Backup;

        // Update/version variables
        public string Update_Channel;
        Update.Update Update_File               = new Update.Update();

        // Timer variables
        public int Backup_Time;
        public int currentHour;
        public int currentMinute;
        public int currentSecond;
        public int Number_Local                 = 0;
        public int Number_Global                = Convert.ToInt32(Properties.Settings.Default.Settings_Backup_Number);
        public bool Minutes                     = true;
        public DateTime EndOfTime;
        private System.Windows.Forms.Timer t    = new System.Windows.Forms.Timer();

        #endregion

        public Main()
        {
            // Initialise components
            InitializeComponent();

            // Set GUI size
            this.Size                   = new Size(386, 500);
            Panel_Settings.Left         = 12;
            Panel_Running.Left          = 0;
            Panel_Running.Top           = 0;

            // Load settings into textboxes
            Main_Textbox_Source.Text    = Properties.Settings.Default.Main_Textbox_Source;
            Main_Textbox_Target.Text    = Properties.Settings.Default.Main_Textbox_Target;
            Main_Textbox_Version.Text   = "Your version: " + Properties.Resources.Version_Main;

            // Check path boxes for existance
            if (!File.Exists(Main_Textbox_Source.Text))
            {
                Main_Textbox_Source.Text                        = "";
                Properties.Settings.Default.Main_Textbox_Source = "";
            }

            // Update check
            if (Properties.Settings.Default.Settings_Check_Autoupdate)
            {
                Update_Start();
            }
        }

        #region Main screen

            #region Button actions

                // Select source path
                private void Main_Button_Source_Click(object sender, EventArgs e)
                {
                    // Initialize dialog
                    FolderBrowserDialog Folderdialog    = new FolderBrowserDialog();
                    Folderdialog.Description            = "Please select the folder to backup. At the moment it's not possible to backup some files only. This will be updated!";
                    Folderdialog.ShowNewFolderButton    = true;

                    // Show dialog and wait result
                    if (Folderdialog.ShowDialog() == DialogResult.OK)
                    {
                        // Set information to textbox
                        Main_Textbox_Source.Text        = "";
                        Main_Textbox_Source.Font        = new Font(Main_Textbox_Source.Font, FontStyle.Regular);
                        Main_Textbox_Source.Text        = Folderdialog.SelectedPath;
                    }
                }

                // Select destination path
                private void Main_Button_Target_Click(object sender, EventArgs e)
                {
                    // Initialize dialog
                    FolderBrowserDialog Folderdialog    = new FolderBrowserDialog();
                    Folderdialog.Description            = "Please select the destination to store your backup to.";
                    Folderdialog.ShowNewFolderButton    = true;

                    // Show dialog wait result
                    if (Folderdialog.ShowDialog() == DialogResult.OK)
                    {
                        // Set information to textbox
                        Main_Textbox_Target.Text        = "";
                        Main_Textbox_Target.Font        = new Font(Main_Textbox_Target.Font, FontStyle.Regular);
                        Main_Textbox_Target.Text        = Folderdialog.SelectedPath;
                    }
                }

                // Open update form
                private void Main_Button_Update_Click(object sender, EventArgs e)
                {
                    // Initialize update form
                    Update_Form Updatef = new Update_Form();

                    // Show update form
                    Updatef.Show();
                }

                // Go to settings screen
                private void Main_Button_Settings_Click(object sender, EventArgs e)
                {
                    // Set GUI changes
                    Panel_Main.Hide();
                    Panel_Settings.Show();

                    // Set the settings
                    // Option selection
                    if (Properties.Settings.Default.Settings_Timer_Option1 == true | Properties.Settings.Default.Settings_Timer_Option2 == false)
                    {
                        // Option 1 enabled

                        // Panel enabling
                        Panel_Time_Option1.Enabled = true;
                        Panel_Time_Option2.Enabled = false;

                        // Set radio state
                        Settings_Radio_Option1.Checked = true;
                        Settings_Radio_Option2.Checked = false;
                    }
                    else if (Properties.Settings.Default.Settings_Timer_Option1 == false | Properties.Settings.Default.Settings_Timer_Option2 == true)
                    {
                        // Option 2 enabled

                        // Panel enabling
                        Panel_Time_Option1.Enabled = false;
                        Panel_Time_Option2.Enabled = true;

                        // Set radio state
                        Settings_Radio_Option1.Checked = false;
                        Settings_Radio_Option2.Checked = true;
                    }

                    // Checkbox everyday
                    if (Properties.Settings.Default.Settings_Check_Everyday == true)
                    {
                        // Checkstate = true

                        // Gui changes
                        Settings_Combo_Day.Enabled      = false;
                        Settings_Check_Everyday.Checked = true;
                    }

                    // Auto backup option
                    if (Properties.Settings.Default.Settings_Check_Autobackup)
                    {
                        // Disable all timer options
                        Panel_Time_Option1.Enabled      = false;
                        Panel_Time_Option2.Enabled      = false;
                        Settings_Radio_Option1.Enabled  = false;
                        Settings_Radio_Option2.Enabled  = false;
                    }

                    // Set text to boxes
                    Settings_Combo_Day.Text         = Properties.Settings.Default.Settings_Combo_Day;
                    Settings_Combo_Selection.Text   = Properties.Settings.Default.Settings_Combo_Selection;
                    Settings_Combo_Time.Text        = Properties.Settings.Default.Settings_Combo_Time;
                    Settings_Textbox_Time.Text      = Properties.Settings.Default.Settings_Textbox_Time;

                    // Set check boxes
                    Settings_Check_Autobackup.Checked = Properties.Settings.Default.Settings_Check_Autobackup;
                    Settings_Check_Autoupdate.Checked = Properties.Settings.Default.Settings_Check_Autoupdate;
                }

                // Start the backup
                private void Main_Button_Start_Click(object sender, EventArgs e)
                {
                    // Check content
                    if (Main_Textbox_Source.Text == "")
                    {
                        MessageBox.Show("Your source-textbox has no content. Please select your source. Making a backup of nothing isn't very handy.");
                        return;
                    }
                    if (Main_Textbox_Target.Text == "")
                    {
                        MessageBox.Show("Your target-textbox has no content. Please select your target directory. Otherwise I can't store my backup somewhere.");
                        return;
                    }

                    // Start proces
                    StartProcess();
                }

                // Save textbox inputs
                private void Main_Button_Save_Click(object sender, EventArgs e)
                {
                    // Save information
                    Properties.Settings.Default.Path_Folder = Main_Textbox_Source.Text;
                    Properties.Settings.Default.Path_Backup = Main_Textbox_Target.Text;

                    // Show message
                    MessageBox.Show("Information succesfully saved!");
                }

                // Show 'about' screen
                private void Main_Button_About_Click(object sender, EventArgs e)
                {
                    // Initialize help form
                    Help helpf = new Help();

                    // Show help form
                    helpf.Show();
                }
            
                // Double-click refresh update information (1/2)
                private void Main_Textbox_Version_DoubleClick(object sender, EventArgs e)
                {
                    // Perform update check
                    Update_Start();
                }

                // Double-click refresh update information (2/2)
                private void Main_Label_Version_DoubleClick(object sender, EventArgs e)
                {
                    // Perform update check
                    Update_Start();
                }

                // Running screen stop button
                private void Running_Button_Stop_Click(object sender, EventArgs e)
                {
                    // Reset GUI
                    this.Size = new Size(386, 500);
                    Panel_Running.Hide();
                    Panel_Main.Show();

                    // Write log
                    Log_Update("Succesfully stopped backup tasks");
                }

                // Running screen now button
                private void Running_Button_Now_Click(object sender, EventArgs e)
                {
                    // Update log
                    Log_Update("Perform backup...");

                    // Make backup
                    Backup("Backup-now button pressed");

                    // Autobackup enabled?
                    if (Properties.Settings.Default.Settings_Check_Autobackup)
                    {
                        // Ask permission
                        var message = MessageBox.Show("Do you want to reset the timer?", "", MessageBoxButtons.YesNo);
                        if (message == System.Windows.Forms.DialogResult.Yes)
                        {
                            // Reset timer
                        }
                    }

                    // Update log
                    Log_Update("Backup succesfull!");
                }

            #endregion

            #region Other actions

                // Textbox source - text changed
                private void Main_Textbox_Source_TextChanged(object sender, EventArgs e)
                {
                    // Save setting
                    Properties.Settings.Default.Main_Textbox_Source = Main_Textbox_Source.Text;
                }

                // Textbox target - text changed
                private void Main_Textbox_Target_TextChanged(object sender, EventArgs e)
                {
                    // Save setting
                    Properties.Settings.Default.Main_Textbox_Target = Main_Textbox_Target.Text;
                }

            #endregion

            #region Tasks

                // New backup task (1/3) - Main task
                private void Backup(string reason)
                {
                    // Update backup numbers
                    Number_Global += 1;
                    Number_Local += 1;

                    // Set paths
                    targetPath = Properties.Settings.Default.Main_Textbox_Target;
                    sourcePath = Properties.Settings.Default.Main_Textbox_Source;

                    // Make the backup
                    try
                    {
                        // Create the backup directory
                        System.IO.Directory.CreateDirectory(targetPath + "\\Backup_Number_" + Convert.ToString(Number_Global));

                        // Copy files to backup directory
                        Copy(sourcePath, targetPath + "\\Backup_Number_" + Convert.ToString(Number_Global));
                    }
                    catch (Exception)
                    {
                        // Show error message
                        MessageBox.Show("The backup has not been made, an error has occured! Please press 'Backup now', restart the program or adress this problem.");

                        // Enable buttons
                        Running_Button_Now.Enabled = true;
                        Running_Button_Stop.Enabled = true;

                        // Stop task
                        return;
                    }

                    // Write to log file
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(targetPath + "\\Log.txt", true))
                    {
                        file.WriteLine("Backup Number: " + Convert.ToString(Number_Global + 1) + ". Backup time: " + DateTime.Now + ". Backup sort: " + reason + ".");
                    }

                    // Update saved backup number
                    Properties.Settings.Default.Settings_Backup_Number = Convert.ToString(Number_Global);

                    // Set label text
                    Running_Label_Info.Text = "Next backup in : " + "Backup number (overall) : " + Number_Global + "Backup number (this session) : " + Number_Local;

                    // Enable buttons
                    Running_Button_Now.Enabled = true;
                    Running_Button_Stop.Enabled = true;
                }

                // Old backup task (4/3) - Main task
                private void Backup_old(bool exit, string backup_reason)
                {

                    // Disable all buttons
                    Running_Button_Now.Enabled = false;
                    Running_Button_Stop.Enabled = false;

                    // Functions to disable autobackup if needed
                    if (Auto_Backup_Enabled == true)
                    {
                        // Stop timer
                        t.Stop();

                        // Set timer variables
                        // If timer uses minutes
                        if (Minutes == true)
                        {
                            // Set timer
                            currentSecond = 60;
                            currentMinute = Backup_Time - 1;

                            // Update time label
                            // Running_Label_Time.Text = "Next backup in " + currentMinute + " : " + currentSecond + " minutes.";
                        }
                        // If timer uses hours
                        else if (Minutes == false)
                        {
                            // Set timer
                            currentSecond = 60;
                            currentMinute = 60 - 1;
                            currentHour = Backup_Time - 1;

                            // Update label
                            // Running_Label_Time.Text = "Next backup in " + currentHour + " : " + currentMinute + " : " + currentSecond + " hours.";
                        }
                    }

                    // Make the backup
                    try
                    {
                        // Create the backup directory
                        System.IO.Directory.CreateDirectory(targetPath + "\\Backup_Number_" + Convert.ToString(Number_Global + 1));

                        // Copy files to backup directory
                        Copy(sourcePath, targetPath + "\\Backup_Number_" + Convert.ToString(Number_Global + 1));
                    }
                    catch (Exception)
                    {
                        // Show error message
                        MessageBox.Show("The backup has not been made, an error has occured! Please press 'Backup now', restart the program or adress this problem.");

                        // Check if exit is true/false
                        if (exit == false)
                        {
                            // Enable buttons

                            // Functions to re-enable autobackup if needed
                            if (Auto_Backup_Enabled == true)
                            {

                                // Start timer
                                t.Start();
                            }
                        }

                        return;
                    }

                    // Write to log file
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(targetPath + "\\Log.txt", true))
                    {
                        file.WriteLine("Backup Number: " + Convert.ToString(Number_Global + 1) + ". Backup time: " + DateTime.Now + ". Backup sort: " + backup_reason + ".");
                    }

                    // Update backup numbers
                    Number_Global += 1;
                    Number_Local += 1;

                    // Update saved backup number
                    Properties.Settings.Default.Settings_Backup_Number = Convert.ToString(Number_Global);

                    // Update labels
                    // Running_Number_Global.Text = "Backup number overall: " + Convert.ToString(Number_Global) + ".";
                    // Running_Number_Local.Text = "Backup number of this session: " + Convert.ToString(Number_Local) + ".";

                    // Check if exit is true/false
                    if (exit == false)
                    {
                        // Enable buttons

                        // Functions to re-enable autobackup if needed
                        if (Auto_Backup_Enabled == true)
                        {
                            // Start timer
                            t.Start();
                        }
                    }
                }

                // Backup task (2/3) - Additive task
                public static void Copy(string sourceDirectory, string targetDirectory)
                {
                    // Convert string information
                    DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
                    DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);

                    // Execute backup
                    CopyAll(diSource, diTarget);
                }

                // Backup task (3/3) - Additive task
                public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
                {
                    // Check if the target directory exists, if not, create it.
                    if (Directory.Exists(target.FullName) == false)
                    {
                        Directory.CreateDirectory(target.FullName);
                    }

                    // Copy each file into it's new directory.
                    foreach (FileInfo fi in source.GetFiles())
                    {
                        fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
                    }

                    // Copy each subdirectory using recursion.
                    foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
                    {
                        DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                        CopyAll(diSourceSubDir, nextTargetSubDir);
                    }
                }

                // Start backup process
                public void StartProcess()
                {
                    // Set GUI changes
                    this.Size = new Size(206, 273);
                    Panel_Running.Show();

                    // Update log
                    Log_Update("GUI loaded, load settings...");

                    // Load settings

                    if (!Properties.Settings.Default.Settings_Check_Autobackup) // Autobackup disabled
                    {
                        // Update log
                        Log_Update("- Auto backup disabled");
                        Log_Update("Settings loaded!");
                        Log_Update("Starting backup task...");

                        // Perform backup task
                        Backup("Start - autobackup disabled");
                    }
                    else if (Properties.Settings.Default.Settings_Check_Autobackup) // Autobackup enabled
                    {
                        // Update log
                        Log_Update("- Auto backup enabled");
                        Log_Update("- Reading backup time");

                        // Initialize backup time settings

                        Log_Update("Settings loaded!");
                        Log_Update("Starting backup task...");

                        // Perform backup task
                        Backup("Start - autobackup enabled");
                    }
                }
                
                // Start update check
                public void Update_Start()
                {
                    // Set GUI changes
                    Main_Image_Loading.Visible = true;

                    // Start update check
                    Update_Check.RunWorkerAsync();
                }
                
                // Set text in log
                public void Log_Update(string text)
                {
                    // Save logbook text
                    string log = Running_Textbox_Log.Text;

                    // Set text to logbook
                    Running_Textbox_Log.Text = text + "\r\n" + log;
                }

            #endregion

            #region Background workers

                // BG  worker strings
                string Version_Latest;

                // Updatecheck (1/2) - Start BG work
                private void Update_Check_DoWork(object sender, DoWorkEventArgs e)
                {
                    // Dowload the information
                    try
                    {
                        // Download version information
                        Version_Latest = this.Update_File.Update_Check("http://www.automaticbackup.nl/" + Properties.Settings.Default.Settings_Updatechannel + "/version");
                    }
                    catch (Exception)
                    {
                        // Error occured
                        Version_Latest = "Error";
                    }
                }
                
                // Updatecheck (2/2) - BG work completed
                private void Update_Check_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
                {
                    // Current version
                    string version_current = Properties.Resources.Version_Main;

                    if (Version_Latest != "Error")
                    {
                        // Set information
                        Main_Textbox_Version.Text = "Your version: " + version_current + "\r\nLatest version: " + Version_Latest;
                    }
                    else if (Version_Latest == "Eror")
                    {
                        // Set information
                        Main_Textbox_Version.Text = "Your version: " + version_current + "\r\nLatest version: unavailable";
                    }

                    // Set GUI changes
                    Main_Image_Loading.Visible = false;
                }

            #endregion

        #endregion

        #region Settings screen

            #region Button actions

                // Back button
                private void Settings_Button_Back_Click(object sender, EventArgs e)
                {
                    // Set GUI changes
                    Panel_Main.Show();
                    Panel_Settings.Hide();
                }

            #endregion

            #region Other actions

                // Autobackup check changed state
                private void Settings_Check_Autobackup_CheckedChanged(object sender, EventArgs e)
                {
                    // Save setting
                    Properties.Settings.Default.Settings_Check_Autobackup = Settings_Check_Autobackup.Checked;

                    // Set GUI changes
                    if (Settings_Check_Autobackup.Checked)
                    {
                        // Disable all timer options
                        Panel_Time_Option1.Enabled      = false;
                        Panel_Time_Option2.Enabled      = false;
                        Settings_Radio_Option1.Enabled  = false;
                        Settings_Radio_Option2.Enabled  = false;
                    }
                    else if (!Settings_Check_Autobackup.Checked)
                    {
                        // Enable all timer options
                        Panel_Time_Option1.Enabled      = true;
                        Panel_Time_Option2.Enabled      = true;
                        Settings_Radio_Option1.Enabled  = true;
                        Settings_Radio_Option2.Enabled  = true;

                        // Option selection
                        if (Properties.Settings.Default.Settings_Timer_Option1 == true | Properties.Settings.Default.Settings_Timer_Option2 == false)
                        {
                            // Option 1 enabled

                            // Panel enabling
                            Panel_Time_Option1.Enabled = true;
                            Panel_Time_Option2.Enabled = false;

                            // Set radio state
                            Settings_Radio_Option1.Checked = true;
                            Settings_Radio_Option2.Checked = false;
                        }
                        else if (Properties.Settings.Default.Settings_Timer_Option1 == false | Properties.Settings.Default.Settings_Timer_Option2 == true)
                        {
                            // Option 2 enabled

                            // Panel enabling
                            Panel_Time_Option1.Enabled = false;
                            Panel_Time_Option2.Enabled = true;

                            // Set radio state
                            Settings_Radio_Option1.Checked = false;
                            Settings_Radio_Option2.Checked = true;
                        }
                    }
                }

                // Autoupdate check changed state
                private void Settings_Check_Autoupdate_CheckedChanged(object sender, EventArgs e)
                {
                    // Save setting
                    Properties.Settings.Default.Settings_Check_Autoupdate = Settings_Check_Autoupdate.Checked;
                }

            #endregion

            #region Timer options

                // Radio option 1
                private void Settings_Radio_Option1_CheckedChanged(object sender, EventArgs e)
                {
                    // GUI changes
                    Panel_Time_Option1.Enabled = true;
                    Panel_Time_Option2.Enabled = false;

                    // Save settings
                    Properties.Settings.Default.Settings_Timer_Option1 = Settings_Radio_Option1.Checked;
                    Properties.Settings.Default.Settings_Timer_Option2 = Settings_Radio_Option2.Checked;
                }

                // Option 1 - Combo selection
                private void Settings_Combo_Selection_SelectedIndexChanged(object sender, EventArgs e)
                {
                    // Save setting
                    Properties.Settings.Default.Settings_Combo_Selection = Settings_Combo_Selection.Text;
                }

                // Option 1 - Textbox time (1/2)
                private void Settings_Textbox_Time_TextChanged(object sender, EventArgs e)
                {
                    // Save setting
                    Properties.Settings.Default.Settings_Textbox_Time = Settings_Textbox_Time.Text;
                }

                // Option 1 - Textbox time (2/2)
                private void Settings_Textbox_Time_KeyPress(object sender, KeyPressEventArgs e)
                {
                    // Digit evaluation
                    e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
                }

                // Radio option 2
                private void Settings_Radio_Option2_CheckedChanged(object sender, EventArgs e)
                {
                    // GUI changes
                    Panel_Time_Option1.Enabled = false;
                    Panel_Time_Option2.Enabled = true;

                    // Save settings
                    Properties.Settings.Default.Settings_Timer_Option1 = Settings_Radio_Option1.Checked;
                    Properties.Settings.Default.Settings_Timer_Option2 = Settings_Radio_Option2.Checked;
                }

                // Option 2 - Everyday check changed
                private void Settings_Check_Everyday_CheckedChanged(object sender, EventArgs e)
                {
                    if (Settings_Check_Everyday.Checked == true)
                    {
                        // GUI changes
                        Settings_Combo_Day.Enabled = false;
                    }
                    else if (Settings_Check_Everyday.Checked == false)
                    {
                        // GUI changes
                        Settings_Combo_Day.Enabled = true;
                    }

                    // Save settings
                    Properties.Settings.Default.Settings_Check_Everyday = Settings_Check_Everyday.Checked;
                }

                // Option 2 - Combo day
                private void Settings_Combo_Day_SelectedIndexChanged(object sender, EventArgs e)
                {
                    // Save setting
                    Properties.Settings.Default.Settings_Combo_Day = Settings_Combo_Day.Text;
                }

                // Option 2 - Combo time
                private void Settings_Combo_Time_SelectedIndexChanged(object sender, EventArgs e)
                {
                    // Save setting
                    Properties.Settings.Default.Settings_Combo_Time = Settings_Combo_Time.Text;
                }

            #endregion

        #endregion
    }
}
