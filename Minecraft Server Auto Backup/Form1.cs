﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Timers;
using System.Net;
using System.Threading;
using Update;

namespace Minecraft_Server_Auto_Backup
{
    public partial class Form1 : Form
    {
    #region initialisation

        //
        // Initialisation
        //

        // IniFile ini = new IniFile(Directory.GetCurrentDirectory() + "\\Settings.ini");
        IniFile Settings                        = new IniFile(Properties.Resources.Settings);
        public string   Server_Folder_Path;
        public string   targetPath;
        public string   sourcePath;
        public bool     Auto_Backup_Enabled     = Properties.Settings.Default.Settings_Auto_Backup;

        // Update/version variables
        public string   Update_Channel;
        Update.Update   Update_File             = new Update.Update();

        // Timer variables
        public int      Backup_Time;
        public int      currentHour;
        public int      currentMinute;
        public int      currentSecond;
        public int      Number_Local            = 0;
        public int      Number_Global           = Convert.ToInt32(Properties.Settings.Default.Settings_Backup_Number);
        public bool     Minutes                 = true;
        public DateTime EndOfTime;
        private System.Windows.Forms.Timer t    = new System.Windows.Forms.Timer();

        public Form1()
        {
            InitializeComponent();

            // Set windows sizes and visibilities
            this.Size                   = new Size(435, 682);
            Panel_Settings.Left         = 12;
            Panel_Settings.BackColor    = Color.Black;
            Panel_Running.Left          = 12;
            Panel_Updating.Left         = 87;
            Panel_Updating.Visible      = true;
            disablehome();

            // Set background color of panels
            if (Properties.Settings.Default.Settings_Back_Color == "") {

                Settings_Combo_Color.Text = "Red";
            }
            else {

                Settings_Combo_Color.Text = Properties.Settings.Default.Settings_Back_Color;
            }
            Change_Back_Color();

            // Fill in textboxes
            Edit_Changes.Text = Properties.Resources.Changes;
            Textbox_Update.Text = "Version info:\r\n\r\nVersion: " + Properties.Resources.Version_Main;

            Textbox_Path_File.Text = Properties.Settings.Default.Path_File; // Read file path from settings
            if (Textbox_Path_File.Text == "") {

                Textbox_Path_File.Text = "Insert the path here or click on '. . .'";
                Textbox_Path_File.Font = new Font(Textbox_Path_File.Font, FontStyle.Italic);
            }

            Textbox_Path_Folder.Text = Properties.Settings.Default.Path_Folder; // Read folder path from settings
            if (Textbox_Path_Folder.Text == "") {

                Textbox_Path_Folder.Text = "Insert the path here or click on '. . .' or just select your server executable";
                Textbox_Path_Folder.Font = new Font(Textbox_Path_Folder.Font, FontStyle.Italic);
            }

            Textbox_Path_Backup.Text = Properties.Settings.Default.Path_Backup; // Read backup path from settings
            if (Textbox_Path_Backup.Text == "") {

                Textbox_Path_Backup.Text = "Insert the path here or click on '. . .'";
                Textbox_Path_Backup.Font = new Font(Textbox_Path_Backup.Font, FontStyle.Italic);
            }

            Settings_Combo_Time.Text = Properties.Settings.Default.Settings_Time_Value;
            if (Settings_Combo_Time.Text == "") {

                Settings_Combo_Time.Text = "minutes";
            }

            // Set timer variables
            t.Interval                              = 1000;
            t.Enabled                               = true;
            t.Tick                                  +=new EventHandler(t_Tick);
            t_Tick(null, null);

            //
            // Set the settings
            //
            // Read backup_time from settings
            Settings_Input_Backuptime.Text          = Properties.Settings.Default.Settings_Backup_Time;
            if (Settings_Input_Backuptime.Text      == "" && Settings_Combo_Time.Text == "minutes") {

                Settings_Input_Backuptime.Text      = "60"; 
                Minutes                             = true;
            }
            else if (Settings_Input_Backuptime.Text == "" && Settings_Combo_Time.Text == "hours") {

                Settings_Input_Backuptime.Text      = "2"; 
                Minutes                             = false;
            }

            // Read backup_number from settings
            Settings_Textbox_Backupnumber.Text      = Properties.Settings.Default.Settings_Backup_Number;
            if (Settings_Textbox_Backupnumber.Text  == "") {

                Settings_Textbox_Backupnumber.Text  = "0";
            }

            // Read need_executable state from settings
            if (Properties.Settings.Default.Settings_Need_Executable == false) {

                Settings_Check_Executable.Checked   = false;
            }
            else {

                Settings_Check_Executable.Checked   = true;
            }

            // Read auto_backup state from settings
            if (Properties.Settings.Default.Settings_Auto_Backup == false) {

                Settings_Check_Backup.Checked       = false;
                Auto_Backup_Enabled                 = false;
            }
            else {

                Settings_Check_Backup.Checked       = true;
                Auto_Backup_Enabled                 = true;
            }

            // Delete update files
            try {

                if (Directory.Exists(Directory.GetCurrentDirectory() + "\\MSAB_Update"))    { Directory.Delete(Directory.GetCurrentDirectory() + "\\MSAB_Update", true); }
                if (File.Exists(Directory.GetCurrentDirectory() + "\\MSAB - Updater.exe"))  { File.Delete(Directory.GetCurrentDirectory() + "\\MSAB - Updater.exe"); }
                if (File.Exists(Directory.GetCurrentDirectory() + "\\MSAB_Updater.exe"))    { File.Delete(Directory.GetCurrentDirectory() + "\\MSAB_Updater.exe"); }
                if (File.Exists(Directory.GetCurrentDirectory() + "\\MSAB - Upgrader.exe")) { File.Delete(Directory.GetCurrentDirectory() + "\\MSAB - Upgrader.exe"); }
                if (File.Exists(Directory.GetCurrentDirectory() + "\\Updater.exe"))         { File.Delete(Directory.GetCurrentDirectory() + "\\Updater.exe"); }
                if (File.Exists(Directory.GetCurrentDirectory() + "\\Info.ini"))            { File.Delete(Directory.GetCurrentDirectory() + "\\Info.ini"); }
            }
            catch (Exception) {

                MessageBox.Show("Some file(s) could not be deleted.");
            }

            // Download version info
            Ping_Version.RunWorkerAsync();
        }
        
        #endregion
        //
        //
    #region Selection panels
        //
        //
        #region File selection panel

                // Click in textbox
            private void Textbox_Path_File_Click(object sender, EventArgs e) {

                if (Textbox_Path_File.Text == "" | Textbox_Path_File.Text == "Insert the path here or click on '. . .'") {
                    Textbox_Path_File.Text = "";
                    Textbox_Path_File.Font = new Font(Textbox_Path_File.Font, FontStyle.Regular);
                }
            }

                // Textbox left
            private void Textbox_Path_File_Leave(object sender, EventArgs e) {

                if (Textbox_Path_File.Text == "" ) {
                    Textbox_Path_File.Text = "Insert the path here or click on '. . .'";
                    Textbox_Path_File.Font = new Font(Textbox_Path_File.Font, FontStyle.Italic);
                }
            }

                // Select file - button pressed
            private void Button_Path_File_Select_Click(object sender, EventArgs e) {

                OpenFileDialog File_Dialog  = new OpenFileDialog();
                File_Dialog.CheckFileExists = true;
                File_Dialog.CheckPathExists = true;
                File_Dialog.AddExtension    = true;
                File_Dialog.Multiselect     = false;
                File_Dialog.Title           = "Select your Minecraft server executable";
                File_Dialog.Filter          = "Executable file (*.exe)|*.exe|All files (*.*)|*.*";

                if (File_Dialog.ShowDialog() == DialogResult.OK) {

                    if (File_Dialog.FileName.EndsWith(".exe")) {

                        Textbox_Path_File.Text = "";
                        Textbox_Path_File.Font = new Font(Textbox_Path_File.Font, FontStyle.Regular);
                        Textbox_Path_File.Text = File_Dialog.FileName;

                        if (!Textbox_Path_Folder.Text.Contains("Insert the path here or click on '. . .' or just select your server executable")) {

                            Textbox_Path_Folder.Text = "";
                            Textbox_Path_Folder.Font = new Font(Textbox_Path_Folder.Font, FontStyle.Regular);
                        }

                        string test = File_Dialog.FileName;

                        if (test.Contains(File_Dialog.SafeFileName)) {
                            test = test.Replace("\\" + File_Dialog.SafeFileName, "");
                        }
                        Textbox_Path_Folder.Text = test;
                        Server_Folder_Path = test;
                    }
                    else {

                        MessageBox.Show("The file you selected is not an executable file, please select one or disable this option in the settings screen. (ends with .exe)");
                    }
                }

            }

                // Button clear pressed
            private void Button_Clear_File_Click(object sender, EventArgs e) {

                Textbox_Path_File.Text = "Insert the path here or click on '. . .'";
                Textbox_Path_File.Font = new Font(Textbox_Path_File.Font, FontStyle.Italic);
            }

        #endregion
        //
        //
        #region Server directory panel

                // Click in textbox
            private void Textbox_Path_Folder_Click(object sender, EventArgs e) {

                if (Textbox_Path_Folder.Text == "" | Textbox_Path_Folder.Text == "Insert the path here or click on '. . .' or just select your server executable") {
                    Textbox_Path_Folder.Text = "";
                    Textbox_Path_Folder.Font = new Font(Textbox_Path_Folder.Font, FontStyle.Regular);
                }
            }

                // Textbox left
            private void Textbox_Path_Folder_Leave(object sender, EventArgs e) {

                if (Textbox_Path_Folder.Text == "") {
                    Textbox_Path_Folder.Text = "Insert the path here or click on '. . .' or just select your server executable";
                    Textbox_Path_Folder.Font = new Font(Textbox_Path_Folder.Font, FontStyle.Italic);
                }
            }

                // Select folder - button pressed
            private void Button_Path_Folder_Select_Click(object sender, EventArgs e) {

                FolderBrowserDialog Folderdialog = new FolderBrowserDialog();
                Folderdialog.Description = "Select the directory containing your server";
                Folderdialog.ShowNewFolderButton = true;

                if (Folderdialog.ShowDialog() == DialogResult.OK) {

                    Textbox_Path_Folder.Text = "";
                    Textbox_Path_Folder.Font = new Font(Textbox_Path_Folder.Font, FontStyle.Regular);

                    Textbox_Path_Folder.Text = Folderdialog.SelectedPath;
                }
            }
        
                // Button clear pressed
            private void Button_Clear_Folder_Click(object sender, EventArgs e) {

                Textbox_Path_Folder.Text = "Insert the path here or click on '. . .' or just select your server executable";
                Textbox_Path_Folder.Font = new Font(Textbox_Path_Folder.Font, FontStyle.Italic);
            }

        #endregion
        //
        //
        #region Backup directory panel

                // Click in textbox
            private void Textbox_Path_Backup_Click(object sender, EventArgs e) {

                if (Textbox_Path_Backup.Text == "" | Textbox_Path_Backup.Text == "Insert the path here or click on '. . .'") {
                    Textbox_Path_Backup.Text = "";
                    Textbox_Path_Backup.Font = new Font(Textbox_Path_Backup.Font, FontStyle.Regular);
                }
            }

                // Textbox left
            private void Textbox_Path_Backup_Leave(object sender, EventArgs e) {

                if (Textbox_Path_Backup.Text == "") {
                    Textbox_Path_Backup.Text = "Insert the path here or click on '. . .'";
                    Textbox_Path_Backup.Font = new Font(Textbox_Path_Backup.Font, FontStyle.Italic);
                }
            }

                // Select folder - button pressed
            private void Button_Path_Backup_Selection_Click(object sender, EventArgs e) {

                FolderBrowserDialog Folderdialog = new FolderBrowserDialog();
                Folderdialog.Description = "Select the directory where the backup can be saved to";
                Folderdialog.ShowNewFolderButton = true;

                if (Folderdialog.ShowDialog() == DialogResult.OK) {

                    Textbox_Path_Backup.Text = "";
                    Textbox_Path_Backup.Font = new Font(Textbox_Path_Backup.Font, FontStyle.Regular);

                    Textbox_Path_Backup.Text = Folderdialog.SelectedPath;
                }
            }

                // Button clear pressed
            private void Button_Clear_Backup_Click(object sender, EventArgs e) {

                Textbox_Path_Backup.Text = "Insert the path here or click on '. . .'";
                Textbox_Path_Backup.Font = new Font(Textbox_Path_Backup.Font, FontStyle.Italic);
            }

        #endregion
        //
        //
    #endregion
        //
        //
    #region Start panel

        private void Button_Start_Click(object sender, EventArgs e)
        {
            // Check input boxes for content
            if (Textbox_Path_Folder.Text == "" | Textbox_Path_Folder.Text == "Insert the path here or click on '. . .' or just select your server executable" | 
                !Directory.Exists(Textbox_Path_Folder.Text) | Textbox_Path_Backup.Text == "" | Textbox_Path_Backup.Text == "Insert the path here or click on '. . .'") {
               
                MessageBox.Show("A startup parameter seems to be missing. Please check everything! (like textboxes etc.)");
                return;
            }

            // Set strings
            sourcePath                          = Textbox_Path_Folder.Text;
            targetPath                          = Textbox_Path_Backup.Text;

            Number_Global                       = Convert.ToInt32(Properties.Settings.Default.Settings_Backup_Number);
            Running_Number_Global.Text          = "Backup number overall: " + Number_Global;

            // Set label information
            if (Auto_Backup_Enabled == true) {

                Backup_Time                     = Convert.ToInt32(Settings_Input_Backuptime.Text);

                if (Settings_Combo_Time.Text == "minutes") {

                    Minutes                     = true;
                    Running_Label_Time.Text     = "Next backup in " + Backup_Time + " : 00 minutes.";
                }
                else if (Settings_Combo_Time.Text == "hours") {

                    Minutes                     = false;
                    Running_Label_Time.Text     = "Next backup in " + Backup_Time + " : 00 : 00 hours.";
                }

                Running_Number_Local.Text       = "Backup number of this session: " + Number_Local + ".";
            }
            else if (Auto_Backup_Enabled == false) {

                Running_Label_Time.Text         = "Auto backup is disabled, new backup on exit.";
                Running_Number_Local.Text       = "";
            }

            // Set GUI visibilities
            Panel_File.Visible                  = false;
            Panel_Server_Folder.Visible         = false;
            Panel_Backup_Folder.Visible         = false;
            Panel_Start.Visible                 = false;
            Panel_Version.Visible               = false;
            Panel_Ad.Visible                    = false;
            Panel_Title.Visible                 = false;

            Panel_Running.Visible               = true;

            // Set GUI size
            this.Size = new Size(280, 180);

            // Save Paths to Settings.ini
            if (!Textbox_Path_File.Text.Contains("Disabled - no executable nessesary")) {

                Properties.Settings.Default.Path_File   = Textbox_Path_File.Text;
            }

            Properties.Settings.Default.Path_Folder     = Textbox_Path_Folder.Text;
            Properties.Settings.Default.Path_Backup     = Textbox_Path_Backup.Text;

            // Perform backup
            Backup(false, "program start");
        }

        private void Button_Settings_Click(object sender, EventArgs e)
        {
            Settings_Textbox_Backupnumber.Text = Properties.Settings.Default.Settings_Backup_Number; // Read backup_number from settings
            if (Settings_Textbox_Backupnumber.Text == "") { Settings_Textbox_Backupnumber.Text = "0"; }

            Panel_File.Visible          = false;
            Panel_Server_Folder.Visible = false;
            Panel_Backup_Folder.Visible = false;
            Panel_Start.Visible         = false;
            Panel_Version.Visible       = false;

            Panel_Settings.Visible      = true;
        }
        
        private void Panel_Ad_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.FileName = "http://bit.ly/zzzu9z";
            process.Start();
        }

#endregion
        //
        //
    #region Settings panel

        private void Settings_Button_Back_Click(object sender, EventArgs e)
        {
            //
            // check for empty textboxes
            //

            // backuptime textbox
            if ((Settings_Input_Backuptime.Text == "" | Settings_Input_Backuptime.Text == " " | Settings_Input_Backuptime.Text == "0") && Auto_Backup_Enabled != false)
            {
                MessageBox.Show("Please enter a backup time before leaving the settings screen or disable the auto-backup function.");
                return;
            }

            // backupnumber textbox
            if (Settings_Textbox_Backupnumber.Text == "")
            {
                MessageBox.Show("Please enter a backup number. If you dont know what to fill in, please enter a 0 (digit).");
                return;
            }

            // time format combobox
            if (Settings_Combo_Time.Text == "")
            {
                MessageBox.Show("Please select a time format! (hours or minutes)");
                return;
            }
            
            //
            // Save all settings
            //
            
            // checkbox 'need executable'
            if (Settings_Check_Executable.Checked == true) { Properties.Settings.Default.Settings_Need_Executable = true; }
            else if (Settings_Check_Executable.Checked == false) { Properties.Settings.Default.Settings_Need_Executable = false; }

            // checkbox 'auto backup'
            if (Settings_Check_Backup.Checked == true) { Properties.Settings.Default.Settings_Auto_Backup = true; }
            else if (Settings_Check_Backup.Checked == false) { Properties.Settings.Default.Settings_Auto_Backup = false; }

            // combobox 'hours/minutes'
            Properties.Settings.Default.Settings_Time_Value = Settings_Combo_Time.Text;

            // Combo color
            Properties.Settings.Default.Settings_Back_Color = Settings_Combo_Color.Text;
            Properties.Settings.Default.Settings_Backup_Time = Settings_Input_Backuptime.Text;
            Properties.Settings.Default.Settings_Backup_Number = Settings_Textbox_Backupnumber.Text;

            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();

            // set label-text
            Running_Label_Time.Text = "Next backup in " + Settings_Input_Backuptime.Text + " " + Settings_Combo_Time.Text + ".";

            //
            // set pannel visibility
            //

            // hide settings panel
            Panel_Settings.Visible = false;

            // show main panels
            Panel_File.Visible = true;
            Panel_Server_Folder.Visible = true;
            Panel_Backup_Folder.Visible = true;
            Panel_Start.Visible = true;
            Panel_Version.Visible = true;
        }

        private void Settings_Input_Backuptime_TextChanged(object sender, EventArgs e)
        {
            if (Settings_Input_Backuptime.Text == "0")
            {
                Settings_Input_Backuptime.MaxLength = 1;
            }
            else
            {
                Settings_Input_Backuptime.MaxLength = 3;
            }
        }

        private void Settings_Input_Backuptime_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void Settings_Textbox_Backupnumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void Settings_Textbox_Backupnumber_TextChanged(object sender, EventArgs e)
        {
            if (Settings_Textbox_Backupnumber.Text == "0")
            {
                Settings_Textbox_Backupnumber.MaxLength = 1;
            }
            else
            {
                Settings_Textbox_Backupnumber.MaxLength = 3;
            }
        }

        private void Settings_Check_Backup_CheckedChanged(object sender, EventArgs e)
        {
            if (Settings_Check_Backup.Checked       == true)  
            {
                Settings_Input_Backuptime.Visible   = true;
                Settings_Label_Backup_1.Visible     = true;
                Settings_Combo_Time.Visible         = true;


                Auto_Backup_Enabled                 = true;
            }
            else if (Settings_Check_Backup.Checked  == false)
            {
                Settings_Input_Backuptime.Visible   = false;
                Settings_Label_Backup_1.Visible     = false;
                Settings_Combo_Time.Visible         = false;

                Auto_Backup_Enabled                 = false;
            }
        }

        private void Settings_Check_Executable_CheckedChanged(object sender, EventArgs e)
        {
            if (Settings_Check_Executable.Checked == false)
            {
                Textbox_Path_File.Enabled       = false;
                Button_Path_File_Select.Enabled = false;
                Button_Clear_File.Enabled       = false;

                Textbox_Path_File.Text = "Insert the path here or click on '. . .'\r\nDisabled - no executable nessesary";
            }
            else if (Settings_Check_Executable.Checked == true)
            {
                Textbox_Path_File.Enabled       = true;
                Button_Path_File_Select.Enabled = true;
                Button_Clear_File.Enabled       = true;
                
                Textbox_Path_File.Text = "Insert the path here or click on '. . .'";
            }
        }

        private void Settings_Combo_Time_TextChanged(object sender, EventArgs e)
        {
            if (Settings_Combo_Time.Text == "minutes") { Settings_Input_Backuptime.Text = "60"; Minutes = true; }
            else if (Settings_Combo_Time.Text == "hours") { Settings_Input_Backuptime.Text = "2"; Minutes = false; }
        }

        private void Settings_Combo_Color_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            Change_Back_Color();

        }

        private void Settings_Radio_Update_Stable_CheckedChanged(object sender, EventArgs e)
        {
            Update_Channel = "Stable";
        }

        private void Settings_Radio_Update_Beta_CheckedChanged(object sender, EventArgs e)
        {
            Update_Channel = "Beta";
        }

        private void Settings_Button_Reset_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure to reset the program?", "", MessageBoxButtons.YesNo);

            if (result == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            Properties.Settings.Default.Path_Backup                 = "";
            Properties.Settings.Default.Path_File                   = "";
            Properties.Settings.Default.Path_Folder                 = "";

            Properties.Settings.Default.Settings_Auto_Backup        = true;
            Properties.Settings.Default.Settings_Back_Color         = "White";
            Properties.Settings.Default.Settings_Backup_Number      = "0";
            Properties.Settings.Default.Settings_Backup_Time        = "60";
            Properties.Settings.Default.Settings_Need_Executable    = false;
            Properties.Settings.Default.Settings_Time_Value         = "minutes";
            Properties.Settings.Default.Settings_Updatechannel      = Properties.Resources.Version_Channel;

            Properties.Settings.Default.Contact_Name                = "";
            Properties.Settings.Default.Contact_Mail                = "";

            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();

            Settings_Check_Executable.Checked = false;
            Settings_Check_Backup.Checked = true;
            Settings_Combo_Color.Text = "White";
            Settings_Input_Backuptime.Text = "60";
            Settings_Textbox_Backupnumber.Text = "0";
            Settings_Combo_Time.Text = "minutes";

            Textbox_Path_File.Text = "Insert the path here or click on '. . .'";
            Textbox_Path_Folder.Text = "Insert the path here or click on '. . .' or just select your server executable";
            Textbox_Path_Backup.Text = "Insert the path here or click on '. . .'";
            
        }

#endregion
        //
        //
    #region Update panel
        
        private void Button_Update_Check_Click(object sender, EventArgs e)
        {

            // Configure update form
            Update_Form update = new Update_Form();

            // Show update form
            update.Show();
        }

        private void Button_Help_Click(object sender, EventArgs e)
        {
            AboutBox1 aboutbox = new AboutBox1();
            //aboutbox.Width = 433;
            aboutbox.Show();
        }

        private void Button_Save_Settings_Click(object sender, EventArgs e)
        {
            // Save Paths to Settings.ini
            if (!Textbox_Path_File.Text.Contains("Disabled - no executable nessesary") | !Textbox_Path_File.Text.Contains("Insert the path here or click on"))
            {
                Properties.Settings.Default.Path_File = Textbox_Path_File.Text;
            }
            else { Properties.Settings.Default.Path_File = ""; }
            if (Textbox_Path_Folder.Text != "Insert the path here or click on '. . .' or just select your server executable") { Properties.Settings.Default.Path_Folder = Textbox_Path_Folder.Text; }
            if (Textbox_Path_Backup.Text != "Insert the path here or click on '. . .'") { Properties.Settings.Default.Path_Backup = Textbox_Path_Backup.Text; }
        }

        private void Textbox_Update_DoubleClick(object sender, EventArgs e)
        {
            disablehome();

            Ping_Version.RunWorkerAsync();
        }

#endregion
        //
        //
    #region Running screen

        private void Running_Button_Backupnow_Click(object sender, EventArgs e)
        {

            // Perform backup
            Backup(false, "clicked 'backup now' button");
        }

        private void Running_Button_Stop_Click(object sender, EventArgs e)
        {

            // Perform backup
            Backup(true, "program exit");

            // Exit program
            Exit_Program();
        }

        private void Running_Button_Return_Click(object sender, EventArgs e)
        {

            // Perform backup
            Backup(true, "back to main");

            // Resize GUI
            this.Size                   = new Size(435, 682);

            // Objects visibility
            Panel_Running.Visible       = false;

            Panel_File.Visible          = true;
            Panel_Server_Folder.Visible = true;
            Panel_Backup_Folder.Visible = true;
            Panel_Start.Visible         = true;
            Panel_Version.Visible       = true;
            Panel_Ad.Visible            = true;
            Panel_Title.Visible         = true;
        }

        private void Running_Label_Ad_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.FileName = "http://bit.ly/zzzu9z";
            process.Start();
        }

#endregion
        //
        //
    #region Tasks
        
        public static void Copy(string sourceDirectory, string targetDirectory)
        {
            // Convert string information
            DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
            DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);

            // Execute backup
            CopyAll(diSource, diTarget);
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(target.FullName) == false) {

                Directory.CreateDirectory(target.FullName);
            }

            // Copy each file into it's new directory.
            foreach (FileInfo fi in source.GetFiles()) {

                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories()) {

                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        private void enablehome()
        {
            Panel_Updating.Visible          = false;

            if (Settings_Check_Executable.Checked == true) {

                Panel_File.Enabled          = true;
            }
            Panel_Backup_Folder.Enabled     = true;
            Panel_Server_Folder.Enabled     = true;
            Panel_Settings.Enabled          = true;
            Panel_Start.Enabled             = true;
            Panel_Title.Enabled             = true;
            Panel_Version.Enabled           = true;
        }

        private void disablehome()
        {
            Panel_Updating.Visible      = true;

            Panel_Backup_Folder.Enabled = false;
            Panel_File.Enabled          = false;
            Panel_Server_Folder.Enabled = false;
            Panel_Settings.Enabled      = false;
            Panel_Start.Enabled         = false;
            Panel_Title.Enabled         = false;
            Panel_Version.Enabled       = false;
        }

        private void Start_Jar()
        {
            // Not available yet
            MessageBox.Show("I am very sorry, but the program can not start '.bat' files corresponding to your server! Please start your server manually.");
        }

        private void Exit_Program()
        {
            // Download_Updater.CancelAsync();
            Ping_Version.CancelAsync();

            // Application exit code
            Application.Exit();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            // Save all settings
            Properties.Settings.Default.Save();

            // Exit program
            Exit_Program();
        }

        private void Change_Back_Color()
        {
            if (Settings_Combo_Color.Text == "Red")
            {
                Panel_Ad.BackColor = Color.Red;
                Panel_Backup_Folder.BackColor = Color.Red;
                Panel_File.BackColor = Color.Red;
                Panel_Running.BackColor = Color.Red;
                Panel_Running_Ad.BackColor = Color.Red;
                Panel_Server_Folder.BackColor = Color.Red;
                Panel_Settings_1.BackColor = Color.Red;
                Panel_Settings_2.BackColor = Color.Red;
                Panel_Start.BackColor = Color.Red;
                Panel_Title.BackColor = Color.Red;
                Panel_Version.BackColor = Color.Red;
            }
            else if (Settings_Combo_Color.Text == "Blue")
            {
                Panel_Ad.BackColor = Color.DodgerBlue;
                Panel_Backup_Folder.BackColor = Color.DodgerBlue;
                Panel_File.BackColor = Color.DodgerBlue;
                Panel_Running.BackColor = Color.DodgerBlue;
                Panel_Running_Ad.BackColor = Color.DodgerBlue;
                Panel_Server_Folder.BackColor = Color.DodgerBlue;
                Panel_Settings_1.BackColor = Color.DodgerBlue;
                Panel_Settings_2.BackColor = Color.DodgerBlue;
                Panel_Start.BackColor = Color.DodgerBlue;
                Panel_Title.BackColor = Color.DodgerBlue;
                Panel_Version.BackColor = Color.DodgerBlue;
            }
            else if (Settings_Combo_Color.Text == "Green")
            {
                Panel_Ad.BackColor = Color.Green;
                Panel_Backup_Folder.BackColor = Color.Green;
                Panel_File.BackColor = Color.Green;
                Panel_Running.BackColor = Color.Green;
                Panel_Running_Ad.BackColor = Color.Green;
                Panel_Server_Folder.BackColor = Color.Green;
                Panel_Settings_1.BackColor = Color.Green;
                Panel_Settings_2.BackColor = Color.Green;
                Panel_Start.BackColor = Color.Green;
                Panel_Title.BackColor = Color.Green;
                Panel_Version.BackColor = Color.Green;
            }
            else if (Settings_Combo_Color.Text == "Purple")
            {
                Panel_Ad.BackColor = Color.Purple;
                Panel_Backup_Folder.BackColor = Color.Purple;
                Panel_File.BackColor = Color.Purple;
                Panel_Running.BackColor = Color.Purple;
                Panel_Running_Ad.BackColor = Color.Purple;
                Panel_Server_Folder.BackColor = Color.Purple;
                Panel_Settings_1.BackColor = Color.Purple;
                Panel_Settings_2.BackColor = Color.Purple;
                Panel_Start.BackColor = Color.Purple;
                Panel_Title.BackColor = Color.Purple;
                Panel_Version.BackColor = Color.Purple;
            }
            else if (Settings_Combo_Color.Text == "White")
            {
                Panel_Ad.BackColor = Color.White;
                Panel_Backup_Folder.BackColor = Color.White;
                Panel_File.BackColor = Color.White;
                Panel_Running.BackColor = Color.White;
                Panel_Running_Ad.BackColor = Color.White;
                Panel_Server_Folder.BackColor = Color.White;
                Panel_Settings_1.BackColor = Color.White;
                Panel_Settings_2.BackColor = Color.White;
                Panel_Start.BackColor = Color.White;
                Panel_Title.BackColor = Color.White;
                Panel_Version.BackColor = Color.White;
            }
            else if (Settings_Combo_Color.Text == "Yellow")
            {
                Panel_Ad.BackColor = Color.Yellow;
                Panel_Backup_Folder.BackColor = Color.Yellow;
                Panel_File.BackColor = Color.Yellow;
                Panel_Running.BackColor = Color.Yellow;
                Panel_Running_Ad.BackColor = Color.Yellow;
                Panel_Server_Folder.BackColor = Color.Yellow;
                Panel_Settings_1.BackColor = Color.Yellow;
                Panel_Settings_2.BackColor = Color.Yellow;
                Panel_Start.BackColor = Color.Yellow;
                Panel_Title.BackColor = Color.Yellow;
                Panel_Version.BackColor = Color.Yellow;
            }
        }
        
        private void t_Tick(object sender, EventArgs e)
        {
            if (Auto_Backup_Enabled == true) {

                // Seconds -1 (timer -1)
                this.currentSecond -= 1;

                // Update time label
                Running_Label_Time.Text = "Next backup in " + currentMinute + " : " + currentSecond + " minutes.";

                // If time is in minutes
                if (currentMinute == 0 && currentSecond == 0 && Minutes == true)
                {

                    // Make backup
                    Backup(false, "automatic backup");

                    return;
                }

                // If hours/minutes/seconds = 0
                if (currentHour == 0 && currentMinute == 0 && currentSecond == 0 && Minutes == false)
                {

                    // Make backup
                    Backup(false, "automatic backup");

                    return;
                }

                // If seconds is lower than 0
                if (currentSecond < 0)
                {

                    // If time is in hours
                    if (Minutes == false)
                    {

                        // If minutes < 0
                        if (currentMinute < 0)
                        {

                            // Hours -1, reset minutes
                            currentMinute = 60;
                            currentHour -= 1;

                            // Update time label
                            Running_Label_Time.Text = "Next backup in " + currentHour + " : " + currentMinute + " : " + currentSecond + " hours.";
                        }
                    }

                    // Minutes -1, reset seconds
                    currentMinute -= 1;
                    currentSecond = 60;

                    // Update time label
                    Running_Label_Time.Text = "Next backup in " + currentMinute + " : " + currentSecond + " minutes.";
                }
            }
        }

        private void Backup(bool exit, string backup_reason)
        {

            // Disable all buttons
            Running_Button_Backupnow.Enabled                    = false;
            Running_Button_Return.Enabled                       = false;
            Running_Button_Stop.Enabled                         = false;

            // Functions to disable autobackup if needed
            if (Auto_Backup_Enabled == true) {

                // Stop timer
                t.Stop();

                // Set timer variables
                    // If timer uses minutes
                if (Minutes == true) {

                    currentSecond                               = 60;
                    currentMinute                               = Backup_Time - 1;

                    Running_Label_Time.Text                     = "Next backup in " + currentMinute + " : " + currentSecond + " minutes.";
                }
                    // If timer uses hours
                else if (Minutes == false) {

                    currentSecond                               = 60;
                    currentMinute                               = 60 - 1;
                    currentHour                                 = Backup_Time - 1;

                    Running_Label_Time.Text                     = "Next backup in " + currentHour + " : " + currentMinute + " : " + currentSecond + " hours.";
                }
            }

            // Make the backup
            try {

                // Create the backup directory
                System.IO.Directory.CreateDirectory(targetPath + "\\Backup_Number_" + Convert.ToString(Number_Global + 1));

                // Copy files to backup directory
                Copy(sourcePath, targetPath + "\\Backup_Number_" + Convert.ToString(Number_Global + 1));
            }
            catch (Exception) {

                // Show error message
                MessageBox.Show("The backup has not been made, an error has occured! Please press 'Backup now', restart the program or contact me (program author).");

                // Check if exit is true/false
                if (exit == false)
                {

                    // Enable buttons
                    Running_Button_Backupnow.Enabled            = true;
                    Running_Button_Return.Enabled               = true;
                    Running_Button_Stop.Enabled                 = true;

                    // Functions to re-enable autobackup if needed
                    if (Auto_Backup_Enabled == true)
                    {

                        // Start timer
                        t.Start();
                    }
                }

                return;
            }

            // Write to log file
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(targetPath + "\\Log.txt", true)) {

                file.WriteLine("Backup Number: " + Convert.ToString(Number_Global + 1) + ". Backup time: " + DateTime.Now + ". Backup sort: " + backup_reason + ".");
            }

            // Update backup numbers
            Number_Global                                       += 1;
            Number_Local                                        += 1;

            // Update saved backup number
            Properties.Settings.Default.Settings_Backup_Number  = Convert.ToString(Number_Global);

            // Update labels
            Running_Number_Global.Text                          = "Backup number overall: " + Convert.ToString(Number_Global) + ".";
            Running_Number_Local.Text                           = "Backup number of this session: " + Convert.ToString(Number_Local) + ".";

            // Check if exit is true/false
            if (exit == false) {

                // Enable buttons
                Running_Button_Backupnow.Enabled                = true;
                Running_Button_Return.Enabled                   = true;
                Running_Button_Stop.Enabled                     = true;

                // Functions to re-enable autobackup if needed
                if (Auto_Backup_Enabled == true){

                    // Start timer
                    t.Start();
                }
            }
        }

#endregion
        //
        //
    #region Background tasks
        //
        // Check version
        //
        // Update strings
        string Version_Latest;
        string Release_Notes_All;
        string Release_Notes_Stable;
        string Release_Notes_Beta;

        private void Ping_Version_DoWork(object sender, DoWorkEventArgs e)
        {

            // Dowload the information
            try {

                    // Download latest version
                Version_Latest = this.Update_File.Update_Check("http://www.automaticbackup.nl/" + Properties.Settings.Default.Settings_Updatechannel + "/version");
                    // Download release notes of all version
                Release_Notes_All = Update_File.Update_Releasenotes("http://www.automaticbackup.nl/" + "/site_resources/html_files/homepage_changes.html");
                    // Download release notes of the current stable version
                Release_Notes_Stable = Update_File.Update_Releasenotes("http://www.automaticbackup.nl/" + "/Stable/releasenotes");
                    // Download release notes of the current beta version
                Release_Notes_Beta = Update_File.Update_Releasenotes("http://www.automaticbackup.nl/" + "/Beta/releasenotes");
            }
            catch (Exception) {

                // Error occured
                Version_Latest = "Error";
            }

        }

        private void Ping_Version_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Local current version
            string version_current = Properties.Resources.Version_Main;

            // Match local current version with downloaded current version
            if (Version_Latest != version_current) {

                // Update available
                this.Textbox_Update.Text = "Version info:\r\nYours:\r\n" + version_current + "\r\nNewest:\r\n" + Version_Latest;
            }
            else if (Version_Latest == version_current | Version_Latest == "Error") {

                // No update
                this.Textbox_Update.Text = "Version info:\r\nYours: " + version_current;
            }

            // Empty changes textbox
            Edit_Changes.Text = "";

            // Set information to changes textbox
            if (Properties.Settings.Default.Settings_Updatechannel == "Beta") {

                Edit_Changes.Text = Release_Notes_Beta + "\r\n\r\n";
            }

            Edit_Changes.Text +=  Release_Notes_Stable + "\r\n" + Release_Notes_All;

            // Enable home again
            enablehome();

        }
#endregion

        private void New_GUI_Click(object sender, EventArgs e)
        {
            Main mainf = new Main();
            mainf.Show();
        }
    }
}