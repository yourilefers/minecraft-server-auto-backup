﻿namespace Minecraft_Server_Auto_Backup
{
    partial class Update_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Update_Form));
            this.Textbox_Releasenotes = new System.Windows.Forms.RichTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.Button_Reload = new System.Windows.Forms.ToolStripMenuItem();
            this.Button_Update = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Button_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.Button_Stable = new System.Windows.Forms.ToolStripMenuItem();
            this.Button_Beta = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.Button_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.Button_About = new System.Windows.Forms.ToolStripMenuItem();
            this.Label_Info = new System.Windows.Forms.Label();
            this.Label_Update = new System.Windows.Forms.Label();
            this.Button_Update2 = new System.Windows.Forms.Button();
            this.Button_Cancel = new System.Windows.Forms.Button();
            this.Label_Releasenotes = new System.Windows.Forms.Label();
            this.Panel_Loading = new System.Windows.Forms.Panel();
            this.Image_Loading = new System.Windows.Forms.PictureBox();
            this.Label_Loading = new System.Windows.Forms.Label();
            this.Back_Update_Check = new System.ComponentModel.BackgroundWorker();
            this.Back_Update = new System.ComponentModel.BackgroundWorker();
            this.toolStrip1.SuspendLayout();
            this.Panel_Loading.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Image_Loading)).BeginInit();
            this.SuspendLayout();
            // 
            // Textbox_Releasenotes
            // 
            this.Textbox_Releasenotes.Location = new System.Drawing.Point(12, 154);
            this.Textbox_Releasenotes.Name = "Textbox_Releasenotes";
            this.Textbox_Releasenotes.ReadOnly = true;
            this.Textbox_Releasenotes.Size = new System.Drawing.Size(260, 195);
            this.Textbox_Releasenotes.TabIndex = 0;
            this.Textbox_Releasenotes.Text = "";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton3,
            this.toolStripDropDownButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(284, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Button_Reload,
            this.Button_Update,
            this.toolStripSeparator1,
            this.Button_Exit});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(38, 22);
            this.toolStripDropDownButton1.Text = "File";
            // 
            // Button_Reload
            // 
            this.Button_Reload.Enabled = false;
            this.Button_Reload.Name = "Button_Reload";
            this.Button_Reload.Size = new System.Drawing.Size(112, 22);
            this.Button_Reload.Text = "Reload";
            this.Button_Reload.Click += new System.EventHandler(this.Button_Reload_Click);
            // 
            // Button_Update
            // 
            this.Button_Update.Enabled = false;
            this.Button_Update.Name = "Button_Update";
            this.Button_Update.Size = new System.Drawing.Size(112, 22);
            this.Button_Update.Text = "Update";
            this.Button_Update.Click += new System.EventHandler(this.Button_Update_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(109, 6);
            // 
            // Button_Exit
            // 
            this.Button_Exit.Name = "Button_Exit";
            this.Button_Exit.Size = new System.Drawing.Size(112, 22);
            this.Button_Exit.Text = "Exit";
            this.Button_Exit.Click += new System.EventHandler(this.Button_Exit_Click);
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Button_Stable,
            this.Button_Beta});
            this.toolStripDropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton3.Image")));
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(100, 22);
            this.toolStripDropDownButton3.Text = "Updatechannel";
            // 
            // Button_Stable
            // 
            this.Button_Stable.CheckOnClick = true;
            this.Button_Stable.Name = "Button_Stable";
            this.Button_Stable.Size = new System.Drawing.Size(106, 22);
            this.Button_Stable.Text = "Stable";
            this.Button_Stable.Click += new System.EventHandler(this.Button_Stable_Click);
            // 
            // Button_Beta
            // 
            this.Button_Beta.CheckOnClick = true;
            this.Button_Beta.Name = "Button_Beta";
            this.Button_Beta.Size = new System.Drawing.Size(106, 22);
            this.Button_Beta.Text = "Beta";
            this.Button_Beta.Click += new System.EventHandler(this.Button_Beta_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Button_Help,
            this.Button_About});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(45, 22);
            this.toolStripDropDownButton2.Text = "Help";
            // 
            // Button_Help
            // 
            this.Button_Help.Name = "Button_Help";
            this.Button_Help.Size = new System.Drawing.Size(152, 22);
            this.Button_Help.Text = "Help";
            this.Button_Help.Click += new System.EventHandler(this.Button_Help_Click);
            // 
            // Button_About
            // 
            this.Button_About.Name = "Button_About";
            this.Button_About.Size = new System.Drawing.Size(152, 22);
            this.Button_About.Text = "About ...";
            this.Button_About.Click += new System.EventHandler(this.Button_About_Click);
            // 
            // Label_Info
            // 
            this.Label_Info.AutoSize = true;
            this.Label_Info.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Info.Location = new System.Drawing.Point(12, 25);
            this.Label_Info.Name = "Label_Info";
            this.Label_Info.Size = new System.Drawing.Size(109, 34);
            this.Label_Info.TabIndex = 3;
            this.Label_Info.Text = "Current version:\r\nNewest version:";
            // 
            // Label_Update
            // 
            this.Label_Update.AutoSize = true;
            this.Label_Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Update.Location = new System.Drawing.Point(12, 77);
            this.Label_Update.Name = "Label_Update";
            this.Label_Update.Size = new System.Drawing.Size(158, 17);
            this.Label_Update.TabIndex = 4;
            this.Label_Update.Text = "Do you want to update?";
            // 
            // Button_Update2
            // 
            this.Button_Update2.Enabled = false;
            this.Button_Update2.Location = new System.Drawing.Point(12, 97);
            this.Button_Update2.Name = "Button_Update2";
            this.Button_Update2.Size = new System.Drawing.Size(75, 23);
            this.Button_Update2.TabIndex = 5;
            this.Button_Update2.Text = "Update";
            this.Button_Update2.UseVisualStyleBackColor = true;
            this.Button_Update2.Click += new System.EventHandler(this.Button_Update2_Click);
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Enabled = false;
            this.Button_Cancel.Location = new System.Drawing.Point(93, 97);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 6;
            this.Button_Cancel.Text = "Cancel";
            this.Button_Cancel.UseVisualStyleBackColor = true;
            this.Button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // Label_Releasenotes
            // 
            this.Label_Releasenotes.AutoSize = true;
            this.Label_Releasenotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Releasenotes.Location = new System.Drawing.Point(12, 134);
            this.Label_Releasenotes.Name = "Label_Releasenotes";
            this.Label_Releasenotes.Size = new System.Drawing.Size(99, 17);
            this.Label_Releasenotes.TabIndex = 7;
            this.Label_Releasenotes.Text = "Release notes";
            // 
            // Panel_Loading
            // 
            this.Panel_Loading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Loading.Controls.Add(this.Image_Loading);
            this.Panel_Loading.Controls.Add(this.Label_Loading);
            this.Panel_Loading.Location = new System.Drawing.Point(42, 120);
            this.Panel_Loading.Name = "Panel_Loading";
            this.Panel_Loading.Size = new System.Drawing.Size(200, 115);
            this.Panel_Loading.TabIndex = 8;
            // 
            // Image_Loading
            // 
            this.Image_Loading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Image_Loading.Image = global::Minecraft_Server_Auto_Backup.Properties.Resources.ajax_loader2;
            this.Image_Loading.Location = new System.Drawing.Point(75, 54);
            this.Image_Loading.Name = "Image_Loading";
            this.Image_Loading.Size = new System.Drawing.Size(48, 48);
            this.Image_Loading.TabIndex = 1;
            this.Image_Loading.TabStop = false;
            // 
            // Label_Loading
            // 
            this.Label_Loading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Loading.Location = new System.Drawing.Point(3, 7);
            this.Label_Loading.Name = "Label_Loading";
            this.Label_Loading.Size = new System.Drawing.Size(192, 44);
            this.Label_Loading.TabIndex = 0;
            this.Label_Loading.Text = "Loading ...";
            this.Label_Loading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Back_Update_Check
            // 
            this.Back_Update_Check.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Back_Update_Check_DoWork);
            this.Back_Update_Check.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Back_Update_Check_RunWorkerCompleted);
            // 
            // Back_Update
            // 
            this.Back_Update.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Back_Update_DoWork);
            this.Back_Update.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Back_Update_RunWorkerCompleted);
            // 
            // Update_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 361);
            this.Controls.Add(this.Panel_Loading);
            this.Controls.Add(this.Label_Releasenotes);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Update2);
            this.Controls.Add(this.Label_Update);
            this.Controls.Add(this.Label_Info);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.Textbox_Releasenotes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Update_Form";
            this.Text = "MSAB - Update";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Update_Form_FormClosing);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.Panel_Loading.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Image_Loading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox Textbox_Releasenotes;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem Button_Reload;
        private System.Windows.Forms.ToolStripMenuItem Button_Update;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem Button_Exit;
        private System.Windows.Forms.Label Label_Info;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem Button_Help;
        private System.Windows.Forms.ToolStripMenuItem Button_About;
        private System.Windows.Forms.Label Label_Update;
        private System.Windows.Forms.Button Button_Update2;
        private System.Windows.Forms.Button Button_Cancel;
        private System.Windows.Forms.Label Label_Releasenotes;
        private System.Windows.Forms.Panel Panel_Loading;
        private System.Windows.Forms.PictureBox Image_Loading;
        private System.Windows.Forms.Label Label_Loading;
        private System.ComponentModel.BackgroundWorker Back_Update_Check;
        private System.ComponentModel.BackgroundWorker Back_Update;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem Button_Stable;
        private System.Windows.Forms.ToolStripMenuItem Button_Beta;
    }
}