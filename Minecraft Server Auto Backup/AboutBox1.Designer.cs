﻿namespace Minecraft_Server_Auto_Backup
{
    partial class AboutBox1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutBox1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Label_Programname = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Label_Programcopyright = new System.Windows.Forms.Label();
            this.Label_Programauthor = new System.Windows.Forms.Label();
            this.Label_Version = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.Textbox = new System.Windows.Forms.RichTextBox();
            this.Button_Homepage = new System.Windows.Forms.Button();
            this.Button_mywebsite = new System.Windows.Forms.Button();
            this.Button_Contact = new System.Windows.Forms.Button();
            this.Panel_Contact = new System.Windows.Forms.Panel();
            this.Panel_Sendmail = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Contact_Button_Back = new System.Windows.Forms.Button();
            this.Contact_Combo_Type = new System.Windows.Forms.ComboBox();
            this.Contact_Button_Submit = new System.Windows.Forms.Button();
            this.Contact_Label_Message = new System.Windows.Forms.Label();
            this.Contact_Textbox_Message = new System.Windows.Forms.RichTextBox();
            this.Contact_Label_Mail = new System.Windows.Forms.Label();
            this.Contact_Textbox_Mail = new System.Windows.Forms.TextBox();
            this.Contact_Label_Name = new System.Windows.Forms.Label();
            this.Contact_Textbox_Name = new System.Windows.Forms.TextBox();
            this.Contact_Label_Title = new System.Windows.Forms.Label();
            this.BGworker_Sendmail = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.Panel_Contact.SuspendLayout();
            this.Panel_Sendmail.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Minecraft_Server_Auto_Backup.Properties.Resources.Title;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(399, 101);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Label_Programname
            // 
            this.Label_Programname.AutoSize = true;
            this.Label_Programname.Location = new System.Drawing.Point(3, 0);
            this.Label_Programname.Name = "Label_Programname";
            this.Label_Programname.Size = new System.Drawing.Size(297, 17);
            this.Label_Programname.TabIndex = 1;
            this.Label_Programname.Text = "Program name: Minecraft Server Auto Backup";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Label_Programcopyright);
            this.panel1.Controls.Add(this.Label_Programauthor);
            this.panel1.Controls.Add(this.Label_Version);
            this.panel1.Controls.Add(this.Label_Programname);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 119);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(399, 75);
            this.panel1.TabIndex = 2;
            // 
            // Label_Programcopyright
            // 
            this.Label_Programcopyright.AutoSize = true;
            this.Label_Programcopyright.Location = new System.Drawing.Point(3, 51);
            this.Label_Programcopyright.Name = "Label_Programcopyright";
            this.Label_Programcopyright.Size = new System.Drawing.Size(219, 17);
            this.Label_Programcopyright.TabIndex = 5;
            this.Label_Programcopyright.Text = "Copyright: (c) Youri Lefers - 2012";
            // 
            // Label_Programauthor
            // 
            this.Label_Programauthor.AutoSize = true;
            this.Label_Programauthor.Location = new System.Drawing.Point(3, 34);
            this.Label_Programauthor.Name = "Label_Programauthor";
            this.Label_Programauthor.Size = new System.Drawing.Size(135, 17);
            this.Label_Programauthor.TabIndex = 4;
            this.Label_Programauthor.Text = "Author: Youri Lefers";
            // 
            // Label_Version
            // 
            this.Label_Version.AutoSize = true;
            this.Label_Version.Location = new System.Drawing.Point(3, 17);
            this.Label_Version.Name = "Label_Version";
            this.Label_Version.Size = new System.Drawing.Size(60, 17);
            this.Label_Version.TabIndex = 2;
            this.Label_Version.Text = "Version:";
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(320, 322);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(95, 23);
            this.okButton.TabIndex = 3;
            this.okButton.Text = "Close";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click_1);
            // 
            // Textbox
            // 
            this.Textbox.Location = new System.Drawing.Point(12, 200);
            this.Textbox.Name = "Textbox";
            this.Textbox.ReadOnly = true;
            this.Textbox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.Textbox.Size = new System.Drawing.Size(302, 145);
            this.Textbox.TabIndex = 4;
            this.Textbox.Text = resources.GetString("Textbox.Text");
            // 
            // Button_Homepage
            // 
            this.Button_Homepage.Location = new System.Drawing.Point(320, 293);
            this.Button_Homepage.Name = "Button_Homepage";
            this.Button_Homepage.Size = new System.Drawing.Size(95, 23);
            this.Button_Homepage.TabIndex = 5;
            this.Button_Homepage.Text = "Project\'s website";
            this.Button_Homepage.UseVisualStyleBackColor = true;
            this.Button_Homepage.Click += new System.EventHandler(this.Button_Homepage_Click);
            // 
            // Button_mywebsite
            // 
            this.Button_mywebsite.Location = new System.Drawing.Point(320, 264);
            this.Button_mywebsite.Name = "Button_mywebsite";
            this.Button_mywebsite.Size = new System.Drawing.Size(95, 23);
            this.Button_mywebsite.TabIndex = 6;
            this.Button_mywebsite.Text = "My website";
            this.Button_mywebsite.UseVisualStyleBackColor = true;
            this.Button_mywebsite.Click += new System.EventHandler(this.Button_mywebsite_Click);
            // 
            // Button_Contact
            // 
            this.Button_Contact.Location = new System.Drawing.Point(320, 235);
            this.Button_Contact.Name = "Button_Contact";
            this.Button_Contact.Size = new System.Drawing.Size(95, 23);
            this.Button_Contact.TabIndex = 7;
            this.Button_Contact.Text = "Contact form";
            this.Button_Contact.UseVisualStyleBackColor = true;
            this.Button_Contact.Click += new System.EventHandler(this.Button_Contact_Click);
            // 
            // Panel_Contact
            // 
            this.Panel_Contact.BackColor = System.Drawing.Color.LightGray;
            this.Panel_Contact.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Contact.Controls.Add(this.Panel_Sendmail);
            this.Panel_Contact.Controls.Add(this.Contact_Button_Back);
            this.Panel_Contact.Controls.Add(this.Contact_Combo_Type);
            this.Panel_Contact.Controls.Add(this.Contact_Button_Submit);
            this.Panel_Contact.Controls.Add(this.Contact_Label_Message);
            this.Panel_Contact.Controls.Add(this.Contact_Textbox_Message);
            this.Panel_Contact.Controls.Add(this.Contact_Label_Mail);
            this.Panel_Contact.Controls.Add(this.Contact_Textbox_Mail);
            this.Panel_Contact.Controls.Add(this.Contact_Label_Name);
            this.Panel_Contact.Controls.Add(this.Contact_Textbox_Name);
            this.Panel_Contact.Controls.Add(this.Contact_Label_Title);
            this.Panel_Contact.Location = new System.Drawing.Point(449, 12);
            this.Panel_Contact.Name = "Panel_Contact";
            this.Panel_Contact.Size = new System.Drawing.Size(263, 333);
            this.Panel_Contact.TabIndex = 8;
            this.Panel_Contact.Visible = false;
            // 
            // Panel_Sendmail
            // 
            this.Panel_Sendmail.BackColor = System.Drawing.Color.Gainsboro;
            this.Panel_Sendmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Sendmail.Controls.Add(this.label1);
            this.Panel_Sendmail.Location = new System.Drawing.Point(56, 106);
            this.Panel_Sendmail.Name = "Panel_Sendmail";
            this.Panel_Sendmail.Size = new System.Drawing.Size(148, 60);
            this.Panel_Sendmail.TabIndex = 10;
            this.Panel_Sendmail.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sending mail ...";
            // 
            // Contact_Button_Back
            // 
            this.Contact_Button_Back.Location = new System.Drawing.Point(0, 0);
            this.Contact_Button_Back.Name = "Contact_Button_Back";
            this.Contact_Button_Back.Size = new System.Drawing.Size(75, 23);
            this.Contact_Button_Back.TabIndex = 9;
            this.Contact_Button_Back.Text = "< Back";
            this.Contact_Button_Back.UseVisualStyleBackColor = true;
            this.Contact_Button_Back.Click += new System.EventHandler(this.Contact_Button_Back_Click);
            // 
            // Contact_Combo_Type
            // 
            this.Contact_Combo_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Contact_Combo_Type.FormattingEnabled = true;
            this.Contact_Combo_Type.Items.AddRange(new object[] {
            "Bug report",
            "Contact",
            "Feature request"});
            this.Contact_Combo_Type.Location = new System.Drawing.Point(4, 307);
            this.Contact_Combo_Type.Name = "Contact_Combo_Type";
            this.Contact_Combo_Type.Size = new System.Drawing.Size(121, 21);
            this.Contact_Combo_Type.TabIndex = 8;
            this.Contact_Combo_Type.SelectedIndexChanged += new System.EventHandler(this.Contact_Combo_Type_SelectedIndexChanged);
            // 
            // Contact_Button_Submit
            // 
            this.Contact_Button_Submit.Location = new System.Drawing.Point(182, 307);
            this.Contact_Button_Submit.Name = "Contact_Button_Submit";
            this.Contact_Button_Submit.Size = new System.Drawing.Size(75, 23);
            this.Contact_Button_Submit.TabIndex = 7;
            this.Contact_Button_Submit.Text = "Submit";
            this.Contact_Button_Submit.UseVisualStyleBackColor = true;
            this.Contact_Button_Submit.Click += new System.EventHandler(this.Contact_Button_Submit_Click);
            // 
            // Contact_Label_Message
            // 
            this.Contact_Label_Message.AutoSize = true;
            this.Contact_Label_Message.Location = new System.Drawing.Point(3, 112);
            this.Contact_Label_Message.Name = "Contact_Label_Message";
            this.Contact_Label_Message.Size = new System.Drawing.Size(74, 13);
            this.Contact_Label_Message.TabIndex = 6;
            this.Contact_Label_Message.Text = "Your message";
            // 
            // Contact_Textbox_Message
            // 
            this.Contact_Textbox_Message.Location = new System.Drawing.Point(4, 128);
            this.Contact_Textbox_Message.Name = "Contact_Textbox_Message";
            this.Contact_Textbox_Message.Size = new System.Drawing.Size(253, 176);
            this.Contact_Textbox_Message.TabIndex = 5;
            this.Contact_Textbox_Message.Text = "";
            // 
            // Contact_Label_Mail
            // 
            this.Contact_Label_Mail.AutoSize = true;
            this.Contact_Label_Mail.Location = new System.Drawing.Point(3, 72);
            this.Contact_Label_Mail.Name = "Contact_Label_Mail";
            this.Contact_Label_Mail.Size = new System.Drawing.Size(84, 13);
            this.Contact_Label_Mail.TabIndex = 4;
            this.Contact_Label_Mail.Text = "Your mail adress";
            // 
            // Contact_Textbox_Mail
            // 
            this.Contact_Textbox_Mail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.Contact_Textbox_Mail.Location = new System.Drawing.Point(4, 88);
            this.Contact_Textbox_Mail.Name = "Contact_Textbox_Mail";
            this.Contact_Textbox_Mail.Size = new System.Drawing.Size(253, 20);
            this.Contact_Textbox_Mail.TabIndex = 3;
            // 
            // Contact_Label_Name
            // 
            this.Contact_Label_Name.AutoSize = true;
            this.Contact_Label_Name.Location = new System.Drawing.Point(3, 29);
            this.Contact_Label_Name.Name = "Contact_Label_Name";
            this.Contact_Label_Name.Size = new System.Drawing.Size(58, 13);
            this.Contact_Label_Name.TabIndex = 2;
            this.Contact_Label_Name.Text = "Your name";
            // 
            // Contact_Textbox_Name
            // 
            this.Contact_Textbox_Name.Location = new System.Drawing.Point(4, 45);
            this.Contact_Textbox_Name.Name = "Contact_Textbox_Name";
            this.Contact_Textbox_Name.Size = new System.Drawing.Size(253, 20);
            this.Contact_Textbox_Name.TabIndex = 1;
            // 
            // Contact_Label_Title
            // 
            this.Contact_Label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Contact_Label_Title.Location = new System.Drawing.Point(112, 0);
            this.Contact_Label_Title.Name = "Contact_Label_Title";
            this.Contact_Label_Title.Size = new System.Drawing.Size(150, 20);
            this.Contact_Label_Title.TabIndex = 0;
            this.Contact_Label_Title.Text = "Contact";
            this.Contact_Label_Title.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // BGworker_Sendmail
            // 
            this.BGworker_Sendmail.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BGworker_Sendmail_DoWork);
            this.BGworker_Sendmail.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BGworker_Sendmail_RunWorkerCompleted);
            // 
            // AboutBox1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 357);
            this.Controls.Add(this.Panel_Contact);
            this.Controls.Add(this.Button_Contact);
            this.Controls.Add(this.Button_mywebsite);
            this.Controls.Add(this.Button_Homepage);
            this.Controls.Add(this.Textbox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutBox1";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Panel_Contact.ResumeLayout(false);
            this.Panel_Contact.PerformLayout();
            this.Panel_Sendmail.ResumeLayout(false);
            this.Panel_Sendmail.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label Label_Programname;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label Label_Version;
        private System.Windows.Forms.Label Label_Programcopyright;
        private System.Windows.Forms.Label Label_Programauthor;
        private System.Windows.Forms.RichTextBox Textbox;
        private System.Windows.Forms.Button Button_Homepage;
        private System.Windows.Forms.Button Button_mywebsite;
        private System.Windows.Forms.Button Button_Contact;
        private System.Windows.Forms.Panel Panel_Contact;
        private System.Windows.Forms.ComboBox Contact_Combo_Type;
        private System.Windows.Forms.Button Contact_Button_Submit;
        private System.Windows.Forms.Label Contact_Label_Message;
        private System.Windows.Forms.RichTextBox Contact_Textbox_Message;
        private System.Windows.Forms.Label Contact_Label_Mail;
        private System.Windows.Forms.TextBox Contact_Textbox_Mail;
        private System.Windows.Forms.Label Contact_Label_Name;
        private System.Windows.Forms.TextBox Contact_Textbox_Name;
        private System.Windows.Forms.Label Contact_Label_Title;
        private System.Windows.Forms.Button Contact_Button_Back;
        private System.ComponentModel.BackgroundWorker BGworker_Sendmail;
        private System.Windows.Forms.Panel Panel_Sendmail;
        private System.Windows.Forms.Label label1;

    }
}
