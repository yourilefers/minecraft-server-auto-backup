﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.544
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Minecraft_Server_Auto_Backup.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Path_Backup {
            get {
                return ((string)(this["Path_Backup"]));
            }
            set {
                this["Path_Backup"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Path_File {
            get {
                return ((string)(this["Path_File"]));
            }
            set {
                this["Path_File"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Path_Folder {
            get {
                return ((string)(this["Path_Folder"]));
            }
            set {
                this["Path_Folder"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Settings_Auto_Backup {
            get {
                return ((bool)(this["Settings_Auto_Backup"]));
            }
            set {
                this["Settings_Auto_Backup"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("White")]
        public string Settings_Back_Color {
            get {
                return ((string)(this["Settings_Back_Color"]));
            }
            set {
                this["Settings_Back_Color"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("0")]
        public string Settings_Backup_Number {
            get {
                return ((string)(this["Settings_Backup_Number"]));
            }
            set {
                this["Settings_Backup_Number"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("60")]
        public string Settings_Backup_Time {
            get {
                return ((string)(this["Settings_Backup_Time"]));
            }
            set {
                this["Settings_Backup_Time"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Settings_Need_Executable {
            get {
                return ((bool)(this["Settings_Need_Executable"]));
            }
            set {
                this["Settings_Need_Executable"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("minutes")]
        public string Settings_Time_Value {
            get {
                return ((string)(this["Settings_Time_Value"]));
            }
            set {
                this["Settings_Time_Value"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Stable")]
        public string Settings_Updatechannel {
            get {
                return ((string)(this["Settings_Updatechannel"]));
            }
            set {
                this["Settings_Updatechannel"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Contact_Name {
            get {
                return ((string)(this["Contact_Name"]));
            }
            set {
                this["Contact_Name"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Contact_Mail {
            get {
                return ((string)(this["Contact_Mail"]));
            }
            set {
                this["Contact_Mail"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Settings_Timer_Option1 {
            get {
                return ((bool)(this["Settings_Timer_Option1"]));
            }
            set {
                this["Settings_Timer_Option1"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Settings_Timer_Option2 {
            get {
                return ((bool)(this["Settings_Timer_Option2"]));
            }
            set {
                this["Settings_Timer_Option2"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("60")]
        public string Settings_Textbox_Time {
            get {
                return ((string)(this["Settings_Textbox_Time"]));
            }
            set {
                this["Settings_Textbox_Time"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Minutes")]
        public string Settings_Combo_Selection {
            get {
                return ((string)(this["Settings_Combo_Selection"]));
            }
            set {
                this["Settings_Combo_Selection"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Monday")]
        public string Settings_Combo_Day {
            get {
                return ((string)(this["Settings_Combo_Day"]));
            }
            set {
                this["Settings_Combo_Day"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("12:00")]
        public string Settings_Combo_Time {
            get {
                return ((string)(this["Settings_Combo_Time"]));
            }
            set {
                this["Settings_Combo_Time"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Settings_Check_Everyday {
            get {
                return ((bool)(this["Settings_Check_Everyday"]));
            }
            set {
                this["Settings_Check_Everyday"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Main_Textbox_Source {
            get {
                return ((string)(this["Main_Textbox_Source"]));
            }
            set {
                this["Main_Textbox_Source"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("")]
        public string Main_Textbox_Target {
            get {
                return ((string)(this["Main_Textbox_Target"]));
            }
            set {
                this["Main_Textbox_Target"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("False")]
        public bool Settings_Check_Autobackup {
            get {
                return ((bool)(this["Settings_Check_Autobackup"]));
            }
            set {
                this["Settings_Check_Autobackup"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool Settings_Check_Autoupdate {
            get {
                return ((bool)(this["Settings_Check_Autoupdate"]));
            }
            set {
                this["Settings_Check_Autoupdate"] = value;
            }
        }
    }
}
