﻿namespace Minecraft_Server_Auto_Backup
{
    partial class Help
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Help));
            this.Panel_Objects = new System.Windows.Forms.Panel();
            this.Main_Label_Subjects = new System.Windows.Forms.Label();
            this.Main_Listbox_Subjects = new System.Windows.Forms.ListBox();
            this.Main_Button_Contact = new System.Windows.Forms.Button();
            this.Main_Button_Home = new System.Windows.Forms.Button();
            this.Main_Button_Exit = new System.Windows.Forms.Button();
            this.Main_Label_Help = new System.Windows.Forms.Label();
            this.Label_Copyright = new System.Windows.Forms.Label();
            this.Panel_Home = new System.Windows.Forms.Panel();
            this.Home_Textbox = new System.Windows.Forms.RichTextBox();
            this.Home_Label_Title = new System.Windows.Forms.Label();
            this.Panel_Subjects = new System.Windows.Forms.Panel();
            this.Subjects_Textbox = new System.Windows.Forms.RichTextBox();
            this.Subjects_Label_Title = new System.Windows.Forms.Label();
            this.Panel_Contact = new System.Windows.Forms.Panel();
            this.Contact_Image_Sending = new System.Windows.Forms.PictureBox();
            this.Contact_Combo_Type = new System.Windows.Forms.ComboBox();
            this.Contact_Label_Type = new System.Windows.Forms.Label();
            this.Contact_Button_Clear = new System.Windows.Forms.Button();
            this.Contact_Button_Send = new System.Windows.Forms.Button();
            this.Contact_Textbox_Message = new System.Windows.Forms.RichTextBox();
            this.Contact_Label_Message = new System.Windows.Forms.Label();
            this.Contact_Textbox_Email = new System.Windows.Forms.TextBox();
            this.Contact_Label_Email = new System.Windows.Forms.Label();
            this.Contact_Textbox_Name = new System.Windows.Forms.TextBox();
            this.Contact_Label_Name = new System.Windows.Forms.Label();
            this.Contact_Label_Title = new System.Windows.Forms.Label();
            this.BG_Sendmessage = new System.ComponentModel.BackgroundWorker();
            this.Panel_Objects.SuspendLayout();
            this.Panel_Home.SuspendLayout();
            this.Panel_Subjects.SuspendLayout();
            this.Panel_Contact.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Contact_Image_Sending)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel_Objects
            // 
            this.Panel_Objects.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Panel_Objects.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Objects.Controls.Add(this.Main_Label_Subjects);
            this.Panel_Objects.Controls.Add(this.Main_Listbox_Subjects);
            this.Panel_Objects.Controls.Add(this.Main_Button_Contact);
            this.Panel_Objects.Controls.Add(this.Main_Button_Home);
            this.Panel_Objects.Controls.Add(this.Main_Button_Exit);
            this.Panel_Objects.Location = new System.Drawing.Point(0, 0);
            this.Panel_Objects.Name = "Panel_Objects";
            this.Panel_Objects.Size = new System.Drawing.Size(137, 460);
            this.Panel_Objects.TabIndex = 0;
            // 
            // Main_Label_Subjects
            // 
            this.Main_Label_Subjects.AutoSize = true;
            this.Main_Label_Subjects.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Main_Label_Subjects.Location = new System.Drawing.Point(11, 67);
            this.Main_Label_Subjects.Name = "Main_Label_Subjects";
            this.Main_Label_Subjects.Size = new System.Drawing.Size(62, 17);
            this.Main_Label_Subjects.TabIndex = 4;
            this.Main_Label_Subjects.Text = "Subjects";
            // 
            // Main_Listbox_Subjects
            // 
            this.Main_Listbox_Subjects.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Main_Listbox_Subjects.FormattingEnabled = true;
            this.Main_Listbox_Subjects.Items.AddRange(new object[] {
            "About",
            "Backup",
            "Backup - make backup",
            "Backup - restore",
            "Settings",
            "Update"});
            this.Main_Listbox_Subjects.Location = new System.Drawing.Point(11, 87);
            this.Main_Listbox_Subjects.Name = "Main_Listbox_Subjects";
            this.Main_Listbox_Subjects.Size = new System.Drawing.Size(119, 342);
            this.Main_Listbox_Subjects.Sorted = true;
            this.Main_Listbox_Subjects.TabIndex = 3;
            this.Main_Listbox_Subjects.SelectedIndexChanged += new System.EventHandler(this.Main_Listbox_Subjects_SelectedIndexChanged);
            // 
            // Main_Button_Contact
            // 
            this.Main_Button_Contact.Location = new System.Drawing.Point(11, 32);
            this.Main_Button_Contact.Name = "Main_Button_Contact";
            this.Main_Button_Contact.Size = new System.Drawing.Size(119, 23);
            this.Main_Button_Contact.TabIndex = 2;
            this.Main_Button_Contact.Text = "Contact";
            this.Main_Button_Contact.UseVisualStyleBackColor = true;
            this.Main_Button_Contact.Click += new System.EventHandler(this.Main_Button_Contact_Click);
            // 
            // Main_Button_Home
            // 
            this.Main_Button_Home.Location = new System.Drawing.Point(11, 3);
            this.Main_Button_Home.Name = "Main_Button_Home";
            this.Main_Button_Home.Size = new System.Drawing.Size(119, 23);
            this.Main_Button_Home.TabIndex = 1;
            this.Main_Button_Home.Text = "Home";
            this.Main_Button_Home.UseVisualStyleBackColor = true;
            this.Main_Button_Home.Click += new System.EventHandler(this.Main_Button_Home_Click);
            // 
            // Main_Button_Exit
            // 
            this.Main_Button_Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Main_Button_Exit.Location = new System.Drawing.Point(11, 432);
            this.Main_Button_Exit.Name = "Main_Button_Exit";
            this.Main_Button_Exit.Size = new System.Drawing.Size(119, 23);
            this.Main_Button_Exit.TabIndex = 0;
            this.Main_Button_Exit.Text = "Exit";
            this.Main_Button_Exit.UseVisualStyleBackColor = true;
            this.Main_Button_Exit.Click += new System.EventHandler(this.Main_Button_Exit_Click);
            // 
            // Main_Label_Help
            // 
            this.Main_Label_Help.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Main_Label_Help.AutoSize = true;
            this.Main_Label_Help.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Main_Label_Help.Location = new System.Drawing.Point(190, 9);
            this.Main_Label_Help.Name = "Main_Label_Help";
            this.Main_Label_Help.Size = new System.Drawing.Size(104, 29);
            this.Main_Label_Help.TabIndex = 1;
            this.Main_Label_Help.Text = "Help me";
            // 
            // Label_Copyright
            // 
            this.Label_Copyright.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Copyright.AutoSize = true;
            this.Label_Copyright.Location = new System.Drawing.Point(163, 439);
            this.Label_Copyright.Name = "Label_Copyright";
            this.Label_Copyright.Size = new System.Drawing.Size(158, 13);
            this.Label_Copyright.TabIndex = 2;
            this.Label_Copyright.Text = "Copyright (c) 2012 - Youri Lefers";
            // 
            // Panel_Home
            // 
            this.Panel_Home.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Panel_Home.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Home.Controls.Add(this.Home_Textbox);
            this.Panel_Home.Controls.Add(this.Home_Label_Title);
            this.Panel_Home.Location = new System.Drawing.Point(143, 41);
            this.Panel_Home.Name = "Panel_Home";
            this.Panel_Home.Size = new System.Drawing.Size(341, 395);
            this.Panel_Home.TabIndex = 3;
            // 
            // Home_Textbox
            // 
            this.Home_Textbox.BackColor = System.Drawing.SystemColors.Control;
            this.Home_Textbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Home_Textbox.Location = new System.Drawing.Point(3, 29);
            this.Home_Textbox.Name = "Home_Textbox";
            this.Home_Textbox.ReadOnly = true;
            this.Home_Textbox.Size = new System.Drawing.Size(333, 359);
            this.Home_Textbox.TabIndex = 1;
            this.Home_Textbox.Text = resources.GetString("Home_Textbox.Text");
            // 
            // Home_Label_Title
            // 
            this.Home_Label_Title.AutoSize = true;
            this.Home_Label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home_Label_Title.Location = new System.Drawing.Point(3, 1);
            this.Home_Label_Title.Name = "Home_Label_Title";
            this.Home_Label_Title.Size = new System.Drawing.Size(64, 25);
            this.Home_Label_Title.TabIndex = 0;
            this.Home_Label_Title.Text = "Home";
            // 
            // Panel_Subjects
            // 
            this.Panel_Subjects.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Panel_Subjects.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Subjects.Controls.Add(this.Subjects_Textbox);
            this.Panel_Subjects.Controls.Add(this.Subjects_Label_Title);
            this.Panel_Subjects.Location = new System.Drawing.Point(490, 41);
            this.Panel_Subjects.Name = "Panel_Subjects";
            this.Panel_Subjects.Size = new System.Drawing.Size(341, 395);
            this.Panel_Subjects.TabIndex = 4;
            this.Panel_Subjects.Visible = false;
            // 
            // Subjects_Textbox
            // 
            this.Subjects_Textbox.BackColor = System.Drawing.SystemColors.Control;
            this.Subjects_Textbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Subjects_Textbox.Location = new System.Drawing.Point(3, 29);
            this.Subjects_Textbox.Name = "Subjects_Textbox";
            this.Subjects_Textbox.ReadOnly = true;
            this.Subjects_Textbox.Size = new System.Drawing.Size(333, 361);
            this.Subjects_Textbox.TabIndex = 2;
            this.Subjects_Textbox.Text = "Lorum ipsum";
            // 
            // Subjects_Label_Title
            // 
            this.Subjects_Label_Title.AutoSize = true;
            this.Subjects_Label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Subjects_Label_Title.Location = new System.Drawing.Point(3, 1);
            this.Subjects_Label_Title.Name = "Subjects_Label_Title";
            this.Subjects_Label_Title.Size = new System.Drawing.Size(88, 25);
            this.Subjects_Label_Title.TabIndex = 1;
            this.Subjects_Label_Title.Text = "Subjects";
            // 
            // Panel_Contact
            // 
            this.Panel_Contact.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Panel_Contact.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Contact.Controls.Add(this.Contact_Image_Sending);
            this.Panel_Contact.Controls.Add(this.Contact_Combo_Type);
            this.Panel_Contact.Controls.Add(this.Contact_Label_Type);
            this.Panel_Contact.Controls.Add(this.Contact_Button_Clear);
            this.Panel_Contact.Controls.Add(this.Contact_Button_Send);
            this.Panel_Contact.Controls.Add(this.Contact_Textbox_Message);
            this.Panel_Contact.Controls.Add(this.Contact_Label_Message);
            this.Panel_Contact.Controls.Add(this.Contact_Textbox_Email);
            this.Panel_Contact.Controls.Add(this.Contact_Label_Email);
            this.Panel_Contact.Controls.Add(this.Contact_Textbox_Name);
            this.Panel_Contact.Controls.Add(this.Contact_Label_Name);
            this.Panel_Contact.Controls.Add(this.Contact_Label_Title);
            this.Panel_Contact.Location = new System.Drawing.Point(837, 41);
            this.Panel_Contact.Name = "Panel_Contact";
            this.Panel_Contact.Size = new System.Drawing.Size(341, 395);
            this.Panel_Contact.TabIndex = 5;
            this.Panel_Contact.Visible = false;
            // 
            // Contact_Image_Sending
            // 
            this.Contact_Image_Sending.Image = global::Minecraft_Server_Auto_Backup.Properties.Resources.ajax_loader2;
            this.Contact_Image_Sending.Location = new System.Drawing.Point(204, 107);
            this.Contact_Image_Sending.Name = "Contact_Image_Sending";
            this.Contact_Image_Sending.Size = new System.Drawing.Size(50, 50);
            this.Contact_Image_Sending.TabIndex = 14;
            this.Contact_Image_Sending.TabStop = false;
            this.Contact_Image_Sending.Visible = false;
            // 
            // Contact_Combo_Type
            // 
            this.Contact_Combo_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Contact_Combo_Type.FormattingEnabled = true;
            this.Contact_Combo_Type.Items.AddRange(new object[] {
            "Contact",
            "Feature request",
            "Bug report"});
            this.Contact_Combo_Type.Location = new System.Drawing.Point(3, 125);
            this.Contact_Combo_Type.Name = "Contact_Combo_Type";
            this.Contact_Combo_Type.Size = new System.Drawing.Size(192, 21);
            this.Contact_Combo_Type.TabIndex = 13;
            // 
            // Contact_Label_Type
            // 
            this.Contact_Label_Type.AutoSize = true;
            this.Contact_Label_Type.Location = new System.Drawing.Point(3, 107);
            this.Contact_Label_Type.Name = "Contact_Label_Type";
            this.Contact_Label_Type.Size = new System.Drawing.Size(82, 13);
            this.Contact_Label_Type.TabIndex = 12;
            this.Contact_Label_Type.Text = "Type of contact";
            // 
            // Contact_Button_Clear
            // 
            this.Contact_Button_Clear.Location = new System.Drawing.Point(260, 107);
            this.Contact_Button_Clear.Name = "Contact_Button_Clear";
            this.Contact_Button_Clear.Size = new System.Drawing.Size(75, 23);
            this.Contact_Button_Clear.TabIndex = 10;
            this.Contact_Button_Clear.Text = "Clear";
            this.Contact_Button_Clear.UseVisualStyleBackColor = true;
            this.Contact_Button_Clear.Click += new System.EventHandler(this.Contact_Button_Clear_Click);
            // 
            // Contact_Button_Send
            // 
            this.Contact_Button_Send.Location = new System.Drawing.Point(260, 136);
            this.Contact_Button_Send.Name = "Contact_Button_Send";
            this.Contact_Button_Send.Size = new System.Drawing.Size(75, 23);
            this.Contact_Button_Send.TabIndex = 9;
            this.Contact_Button_Send.Text = "Send";
            this.Contact_Button_Send.UseVisualStyleBackColor = true;
            this.Contact_Button_Send.Click += new System.EventHandler(this.Contact_Button_Send_Click);
            // 
            // Contact_Textbox_Message
            // 
            this.Contact_Textbox_Message.Location = new System.Drawing.Point(3, 165);
            this.Contact_Textbox_Message.Name = "Contact_Textbox_Message";
            this.Contact_Textbox_Message.Size = new System.Drawing.Size(332, 223);
            this.Contact_Textbox_Message.TabIndex = 8;
            this.Contact_Textbox_Message.Text = "";
            // 
            // Contact_Label_Message
            // 
            this.Contact_Label_Message.AutoSize = true;
            this.Contact_Label_Message.Location = new System.Drawing.Point(3, 149);
            this.Contact_Label_Message.Name = "Contact_Label_Message";
            this.Contact_Label_Message.Size = new System.Drawing.Size(74, 13);
            this.Contact_Label_Message.TabIndex = 7;
            this.Contact_Label_Message.Text = "Your message";
            // 
            // Contact_Textbox_Email
            // 
            this.Contact_Textbox_Email.Location = new System.Drawing.Point(3, 84);
            this.Contact_Textbox_Email.Name = "Contact_Textbox_Email";
            this.Contact_Textbox_Email.Size = new System.Drawing.Size(192, 20);
            this.Contact_Textbox_Email.TabIndex = 6;
            // 
            // Contact_Label_Email
            // 
            this.Contact_Label_Email.AutoSize = true;
            this.Contact_Label_Email.Location = new System.Drawing.Point(3, 68);
            this.Contact_Label_Email.Name = "Contact_Label_Email";
            this.Contact_Label_Email.Size = new System.Drawing.Size(90, 13);
            this.Contact_Label_Email.TabIndex = 5;
            this.Contact_Label_Email.Text = "Your email adress";
            // 
            // Contact_Textbox_Name
            // 
            this.Contact_Textbox_Name.Location = new System.Drawing.Point(3, 45);
            this.Contact_Textbox_Name.Name = "Contact_Textbox_Name";
            this.Contact_Textbox_Name.Size = new System.Drawing.Size(192, 20);
            this.Contact_Textbox_Name.TabIndex = 4;
            // 
            // Contact_Label_Name
            // 
            this.Contact_Label_Name.AutoSize = true;
            this.Contact_Label_Name.Location = new System.Drawing.Point(3, 29);
            this.Contact_Label_Name.Name = "Contact_Label_Name";
            this.Contact_Label_Name.Size = new System.Drawing.Size(58, 13);
            this.Contact_Label_Name.TabIndex = 3;
            this.Contact_Label_Name.Text = "Your name";
            // 
            // Contact_Label_Title
            // 
            this.Contact_Label_Title.AutoSize = true;
            this.Contact_Label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Contact_Label_Title.Location = new System.Drawing.Point(3, 1);
            this.Contact_Label_Title.Name = "Contact_Label_Title";
            this.Contact_Label_Title.Size = new System.Drawing.Size(80, 25);
            this.Contact_Label_Title.TabIndex = 2;
            this.Contact_Label_Title.Text = "Contact";
            // 
            // BG_Sendmessage
            // 
            this.BG_Sendmessage.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BG_Sendmessage_DoWork);
            this.BG_Sendmessage.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BG_Sendmessage_RunWorkerCompleted);
            // 
            // Help
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1185, 461);
            this.Controls.Add(this.Panel_Contact);
            this.Controls.Add(this.Panel_Subjects);
            this.Controls.Add(this.Panel_Home);
            this.Controls.Add(this.Label_Copyright);
            this.Controls.Add(this.Main_Label_Help);
            this.Controls.Add(this.Panel_Objects);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Help";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Help me";
            this.Panel_Objects.ResumeLayout(false);
            this.Panel_Objects.PerformLayout();
            this.Panel_Home.ResumeLayout(false);
            this.Panel_Home.PerformLayout();
            this.Panel_Subjects.ResumeLayout(false);
            this.Panel_Subjects.PerformLayout();
            this.Panel_Contact.ResumeLayout(false);
            this.Panel_Contact.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Contact_Image_Sending)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Panel_Objects;
        private System.Windows.Forms.Label Main_Label_Help;
        private System.Windows.Forms.Label Label_Copyright;
        private System.Windows.Forms.Panel Panel_Home;
        private System.Windows.Forms.Button Main_Button_Home;
        private System.Windows.Forms.Button Main_Button_Exit;
        private System.Windows.Forms.Label Main_Label_Subjects;
        private System.Windows.Forms.ListBox Main_Listbox_Subjects;
        private System.Windows.Forms.Button Main_Button_Contact;
        private System.Windows.Forms.Label Home_Label_Title;
        private System.Windows.Forms.Panel Panel_Subjects;
        private System.Windows.Forms.Panel Panel_Contact;
        private System.Windows.Forms.RichTextBox Home_Textbox;
        private System.Windows.Forms.RichTextBox Subjects_Textbox;
        private System.Windows.Forms.Label Subjects_Label_Title;
        private System.Windows.Forms.Label Contact_Label_Title;
        private System.Windows.Forms.Button Contact_Button_Clear;
        private System.Windows.Forms.Button Contact_Button_Send;
        private System.Windows.Forms.RichTextBox Contact_Textbox_Message;
        private System.Windows.Forms.Label Contact_Label_Message;
        private System.Windows.Forms.TextBox Contact_Textbox_Email;
        private System.Windows.Forms.Label Contact_Label_Email;
        private System.Windows.Forms.TextBox Contact_Textbox_Name;
        private System.Windows.Forms.Label Contact_Label_Name;
        private System.ComponentModel.BackgroundWorker BG_Sendmessage;
        private System.Windows.Forms.ComboBox Contact_Combo_Type;
        private System.Windows.Forms.Label Contact_Label_Type;
        private System.Windows.Forms.PictureBox Contact_Image_Sending;

    }
}