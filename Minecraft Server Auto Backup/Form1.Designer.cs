﻿namespace Minecraft_Server_Auto_Backup
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Panel_File = new System.Windows.Forms.Panel();
            this.Button_Clear_File = new System.Windows.Forms.Button();
            this.Button_Path_File_Select = new System.Windows.Forms.Button();
            this.Textbox_Path_File = new System.Windows.Forms.TextBox();
            this.Lbl_Path_File = new System.Windows.Forms.Label();
            this.Panel_Server_Folder = new System.Windows.Forms.Panel();
            this.Button_Clear_Folder = new System.Windows.Forms.Button();
            this.Button_Path_Folder_Select = new System.Windows.Forms.Button();
            this.Textbox_Path_Folder = new System.Windows.Forms.TextBox();
            this.Lbl_Path_Folder = new System.Windows.Forms.Label();
            this.Panel_Backup_Folder = new System.Windows.Forms.Panel();
            this.Button_Clear_Backup = new System.Windows.Forms.Button();
            this.Button_Path_Backup_Selection = new System.Windows.Forms.Button();
            this.Textbox_Path_Backup = new System.Windows.Forms.TextBox();
            this.Lbl_Path_Backup = new System.Windows.Forms.Label();
            this.Panel_Version = new System.Windows.Forms.Panel();
            this.Button_Help = new System.Windows.Forms.Button();
            this.Button_Update_Check = new System.Windows.Forms.Button();
            this.Textbox_Update = new System.Windows.Forms.TextBox();
            this.Panel_Start = new System.Windows.Forms.Panel();
            this.Button_Settings = new System.Windows.Forms.Button();
            this.Button_Save_Settings = new System.Windows.Forms.Button();
            this.Button_Start = new System.Windows.Forms.Button();
            this.Panel_Settings = new System.Windows.Forms.Panel();
            this.Panel_Settings_1 = new System.Windows.Forms.Panel();
            this.Settings_Button_Reset = new System.Windows.Forms.Button();
            this.Settings_Combo_Time = new System.Windows.Forms.ComboBox();
            this.Settings_Label_Color = new System.Windows.Forms.Label();
            this.Settings_Combo_Color = new System.Windows.Forms.ComboBox();
            this.Settings_Textbox_Backupnumber = new System.Windows.Forms.TextBox();
            this.Settings_Label_Number = new System.Windows.Forms.Label();
            this.Settings_Button_Back = new System.Windows.Forms.Button();
            this.Settings_Label_Backup_1 = new System.Windows.Forms.Label();
            this.Settings_Input_Backuptime = new System.Windows.Forms.TextBox();
            this.Settings_Check_Update = new System.Windows.Forms.CheckBox();
            this.Settings_Check_Backup = new System.Windows.Forms.CheckBox();
            this.Settings_Check_Executable = new System.Windows.Forms.CheckBox();
            this.Settings_Label_Title = new System.Windows.Forms.Label();
            this.Panel_Settings_2 = new System.Windows.Forms.Panel();
            this.Edit_Changes = new System.Windows.Forms.RichTextBox();
            this.Panel_Running = new System.Windows.Forms.Panel();
            this.Running_Button_Backupnow = new System.Windows.Forms.Button();
            this.Running_Button_Return = new System.Windows.Forms.Button();
            this.Running_Button_Stop = new System.Windows.Forms.Button();
            this.Running_Number_Local = new System.Windows.Forms.Label();
            this.Running_Number_Global = new System.Windows.Forms.Label();
            this.Running_Label_Time = new System.Windows.Forms.Label();
            this.Panel_Running_Ad = new System.Windows.Forms.Panel();
            this.Running_Label_Ad = new System.Windows.Forms.LinkLabel();
            this.Panel_Ad = new System.Windows.Forms.Panel();
            this.Panel_Title = new System.Windows.Forms.Panel();
            this.Panel_Updating = new System.Windows.Forms.Panel();
            this.Updating_Image = new System.Windows.Forms.PictureBox();
            this.Updating_Label = new System.Windows.Forms.Label();
            this.Ping_Version = new System.ComponentModel.BackgroundWorker();
            this.Download_Updater = new System.ComponentModel.BackgroundWorker();
            this.New_GUI = new System.Windows.Forms.Button();
            this.Panel_File.SuspendLayout();
            this.Panel_Server_Folder.SuspendLayout();
            this.Panel_Backup_Folder.SuspendLayout();
            this.Panel_Version.SuspendLayout();
            this.Panel_Start.SuspendLayout();
            this.Panel_Settings.SuspendLayout();
            this.Panel_Settings_1.SuspendLayout();
            this.Panel_Settings_2.SuspendLayout();
            this.Panel_Running.SuspendLayout();
            this.Panel_Running_Ad.SuspendLayout();
            this.Panel_Updating.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Updating_Image)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel_File
            // 
            this.Panel_File.BackColor = System.Drawing.Color.Red;
            this.Panel_File.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_File.Controls.Add(this.Button_Clear_File);
            this.Panel_File.Controls.Add(this.Button_Path_File_Select);
            this.Panel_File.Controls.Add(this.Textbox_Path_File);
            this.Panel_File.Controls.Add(this.Lbl_Path_File);
            this.Panel_File.Location = new System.Drawing.Point(12, 118);
            this.Panel_File.Name = "Panel_File";
            this.Panel_File.Size = new System.Drawing.Size(400, 100);
            this.Panel_File.TabIndex = 1;
            // 
            // Button_Clear_File
            // 
            this.Button_Clear_File.Enabled = false;
            this.Button_Clear_File.Location = new System.Drawing.Point(239, 72);
            this.Button_Clear_File.Name = "Button_Clear_File";
            this.Button_Clear_File.Size = new System.Drawing.Size(75, 23);
            this.Button_Clear_File.TabIndex = 3;
            this.Button_Clear_File.Text = "Reset";
            this.Button_Clear_File.UseVisualStyleBackColor = true;
            this.Button_Clear_File.Click += new System.EventHandler(this.Button_Clear_File_Click);
            // 
            // Button_Path_File_Select
            // 
            this.Button_Path_File_Select.Enabled = false;
            this.Button_Path_File_Select.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.Button_Path_File_Select.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Button_Path_File_Select.Location = new System.Drawing.Point(320, 72);
            this.Button_Path_File_Select.Name = "Button_Path_File_Select";
            this.Button_Path_File_Select.Size = new System.Drawing.Size(75, 23);
            this.Button_Path_File_Select.TabIndex = 2;
            this.Button_Path_File_Select.Text = ". . .";
            this.Button_Path_File_Select.UseVisualStyleBackColor = true;
            this.Button_Path_File_Select.Click += new System.EventHandler(this.Button_Path_File_Select_Click);
            // 
            // Textbox_Path_File
            // 
            this.Textbox_Path_File.Enabled = false;
            this.Textbox_Path_File.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Textbox_Path_File.Location = new System.Drawing.Point(3, 21);
            this.Textbox_Path_File.Multiline = true;
            this.Textbox_Path_File.Name = "Textbox_Path_File";
            this.Textbox_Path_File.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Textbox_Path_File.Size = new System.Drawing.Size(392, 45);
            this.Textbox_Path_File.TabIndex = 1;
            this.Textbox_Path_File.Text = "Insert the path here or click on \'. . .\'";
            this.Textbox_Path_File.Click += new System.EventHandler(this.Textbox_Path_File_Click);
            this.Textbox_Path_File.Leave += new System.EventHandler(this.Textbox_Path_File_Leave);
            // 
            // Lbl_Path_File
            // 
            this.Lbl_Path_File.AutoSize = true;
            this.Lbl_Path_File.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.Lbl_Path_File.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Lbl_Path_File.Location = new System.Drawing.Point(3, 0);
            this.Lbl_Path_File.Name = "Lbl_Path_File";
            this.Lbl_Path_File.Size = new System.Drawing.Size(308, 17);
            this.Lbl_Path_File.TabIndex = 0;
            this.Lbl_Path_File.Text = "Select your Minecraft server \"executable\"";
            // 
            // Panel_Server_Folder
            // 
            this.Panel_Server_Folder.BackColor = System.Drawing.Color.Red;
            this.Panel_Server_Folder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Server_Folder.Controls.Add(this.Button_Clear_Folder);
            this.Panel_Server_Folder.Controls.Add(this.Button_Path_Folder_Select);
            this.Panel_Server_Folder.Controls.Add(this.Textbox_Path_Folder);
            this.Panel_Server_Folder.Controls.Add(this.Lbl_Path_Folder);
            this.Panel_Server_Folder.Location = new System.Drawing.Point(12, 224);
            this.Panel_Server_Folder.Name = "Panel_Server_Folder";
            this.Panel_Server_Folder.Size = new System.Drawing.Size(400, 100);
            this.Panel_Server_Folder.TabIndex = 2;
            // 
            // Button_Clear_Folder
            // 
            this.Button_Clear_Folder.Location = new System.Drawing.Point(239, 74);
            this.Button_Clear_Folder.Name = "Button_Clear_Folder";
            this.Button_Clear_Folder.Size = new System.Drawing.Size(75, 23);
            this.Button_Clear_Folder.TabIndex = 7;
            this.Button_Clear_Folder.Text = "Reset";
            this.Button_Clear_Folder.UseVisualStyleBackColor = true;
            this.Button_Clear_Folder.Click += new System.EventHandler(this.Button_Clear_Folder_Click);
            // 
            // Button_Path_Folder_Select
            // 
            this.Button_Path_Folder_Select.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.Button_Path_Folder_Select.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Button_Path_Folder_Select.Location = new System.Drawing.Point(320, 74);
            this.Button_Path_Folder_Select.Name = "Button_Path_Folder_Select";
            this.Button_Path_Folder_Select.Size = new System.Drawing.Size(75, 23);
            this.Button_Path_Folder_Select.TabIndex = 5;
            this.Button_Path_Folder_Select.Text = ". . .";
            this.Button_Path_Folder_Select.UseVisualStyleBackColor = true;
            this.Button_Path_Folder_Select.Click += new System.EventHandler(this.Button_Path_Folder_Select_Click);
            // 
            // Textbox_Path_Folder
            // 
            this.Textbox_Path_Folder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Textbox_Path_Folder.Location = new System.Drawing.Point(3, 23);
            this.Textbox_Path_Folder.Multiline = true;
            this.Textbox_Path_Folder.Name = "Textbox_Path_Folder";
            this.Textbox_Path_Folder.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Textbox_Path_Folder.Size = new System.Drawing.Size(392, 45);
            this.Textbox_Path_Folder.TabIndex = 4;
            this.Textbox_Path_Folder.Text = "Insert the path here or click on \'. . .\' or just select your server executable";
            this.Textbox_Path_Folder.Click += new System.EventHandler(this.Textbox_Path_Folder_Click);
            this.Textbox_Path_Folder.Leave += new System.EventHandler(this.Textbox_Path_Folder_Leave);
            // 
            // Lbl_Path_Folder
            // 
            this.Lbl_Path_Folder.AutoSize = true;
            this.Lbl_Path_Folder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.Lbl_Path_Folder.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Lbl_Path_Folder.Location = new System.Drawing.Point(3, 2);
            this.Lbl_Path_Folder.Name = "Lbl_Path_Folder";
            this.Lbl_Path_Folder.Size = new System.Drawing.Size(329, 17);
            this.Lbl_Path_Folder.TabIndex = 3;
            this.Lbl_Path_Folder.Text = "Select the directory of your Minecraft server";
            // 
            // Panel_Backup_Folder
            // 
            this.Panel_Backup_Folder.BackColor = System.Drawing.Color.Red;
            this.Panel_Backup_Folder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Backup_Folder.Controls.Add(this.Button_Clear_Backup);
            this.Panel_Backup_Folder.Controls.Add(this.Button_Path_Backup_Selection);
            this.Panel_Backup_Folder.Controls.Add(this.Textbox_Path_Backup);
            this.Panel_Backup_Folder.Controls.Add(this.Lbl_Path_Backup);
            this.Panel_Backup_Folder.Location = new System.Drawing.Point(12, 330);
            this.Panel_Backup_Folder.Name = "Panel_Backup_Folder";
            this.Panel_Backup_Folder.Size = new System.Drawing.Size(400, 100);
            this.Panel_Backup_Folder.TabIndex = 3;
            // 
            // Button_Clear_Backup
            // 
            this.Button_Clear_Backup.Location = new System.Drawing.Point(239, 72);
            this.Button_Clear_Backup.Name = "Button_Clear_Backup";
            this.Button_Clear_Backup.Size = new System.Drawing.Size(75, 23);
            this.Button_Clear_Backup.TabIndex = 6;
            this.Button_Clear_Backup.Text = "Reset";
            this.Button_Clear_Backup.UseVisualStyleBackColor = true;
            this.Button_Clear_Backup.Click += new System.EventHandler(this.Button_Clear_Backup_Click);
            // 
            // Button_Path_Backup_Selection
            // 
            this.Button_Path_Backup_Selection.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.Button_Path_Backup_Selection.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Button_Path_Backup_Selection.Location = new System.Drawing.Point(320, 72);
            this.Button_Path_Backup_Selection.Name = "Button_Path_Backup_Selection";
            this.Button_Path_Backup_Selection.Size = new System.Drawing.Size(75, 23);
            this.Button_Path_Backup_Selection.TabIndex = 5;
            this.Button_Path_Backup_Selection.Text = ". . .";
            this.Button_Path_Backup_Selection.UseVisualStyleBackColor = true;
            this.Button_Path_Backup_Selection.Click += new System.EventHandler(this.Button_Path_Backup_Selection_Click);
            // 
            // Textbox_Path_Backup
            // 
            this.Textbox_Path_Backup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Textbox_Path_Backup.Location = new System.Drawing.Point(3, 23);
            this.Textbox_Path_Backup.Multiline = true;
            this.Textbox_Path_Backup.Name = "Textbox_Path_Backup";
            this.Textbox_Path_Backup.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Textbox_Path_Backup.Size = new System.Drawing.Size(392, 45);
            this.Textbox_Path_Backup.TabIndex = 4;
            this.Textbox_Path_Backup.Text = "Insert the path here or click on \'. . .\'";
            this.Textbox_Path_Backup.Click += new System.EventHandler(this.Textbox_Path_Backup_Click);
            this.Textbox_Path_Backup.Leave += new System.EventHandler(this.Textbox_Path_Backup_Leave);
            // 
            // Lbl_Path_Backup
            // 
            this.Lbl_Path_Backup.AutoSize = true;
            this.Lbl_Path_Backup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.Lbl_Path_Backup.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Lbl_Path_Backup.Location = new System.Drawing.Point(3, 2);
            this.Lbl_Path_Backup.Name = "Lbl_Path_Backup";
            this.Lbl_Path_Backup.Size = new System.Drawing.Size(291, 17);
            this.Lbl_Path_Backup.TabIndex = 3;
            this.Lbl_Path_Backup.Text = "Select a location to save the backup to";
            // 
            // Panel_Version
            // 
            this.Panel_Version.BackColor = System.Drawing.Color.Red;
            this.Panel_Version.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Version.Controls.Add(this.New_GUI);
            this.Panel_Version.Controls.Add(this.Button_Help);
            this.Panel_Version.Controls.Add(this.Button_Update_Check);
            this.Panel_Version.Controls.Add(this.Textbox_Update);
            this.Panel_Version.Location = new System.Drawing.Point(12, 436);
            this.Panel_Version.Name = "Panel_Version";
            this.Panel_Version.Size = new System.Drawing.Size(195, 100);
            this.Panel_Version.TabIndex = 4;
            // 
            // Button_Help
            // 
            this.Button_Help.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Button_Help.Location = new System.Drawing.Point(115, 72);
            this.Button_Help.Name = "Button_Help";
            this.Button_Help.Size = new System.Drawing.Size(75, 23);
            this.Button_Help.TabIndex = 3;
            this.Button_Help.Text = "About";
            this.Button_Help.UseVisualStyleBackColor = true;
            this.Button_Help.Click += new System.EventHandler(this.Button_Help_Click);
            // 
            // Button_Update_Check
            // 
            this.Button_Update_Check.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Button_Update_Check.Location = new System.Drawing.Point(115, 3);
            this.Button_Update_Check.Name = "Button_Update_Check";
            this.Button_Update_Check.Size = new System.Drawing.Size(75, 23);
            this.Button_Update_Check.TabIndex = 1;
            this.Button_Update_Check.Text = "Update";
            this.Button_Update_Check.UseVisualStyleBackColor = true;
            this.Button_Update_Check.Click += new System.EventHandler(this.Button_Update_Check_Click);
            // 
            // Textbox_Update
            // 
            this.Textbox_Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Textbox_Update.Location = new System.Drawing.Point(6, 3);
            this.Textbox_Update.Multiline = true;
            this.Textbox_Update.Name = "Textbox_Update";
            this.Textbox_Update.ReadOnly = true;
            this.Textbox_Update.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Textbox_Update.Size = new System.Drawing.Size(103, 92);
            this.Textbox_Update.TabIndex = 0;
            this.Textbox_Update.Text = "Version info:\r\n\r\nVersion: 3.0\r\n(Bèta)";
            this.Textbox_Update.DoubleClick += new System.EventHandler(this.Textbox_Update_DoubleClick);
            // 
            // Panel_Start
            // 
            this.Panel_Start.BackColor = System.Drawing.Color.Red;
            this.Panel_Start.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Panel_Start.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Start.Controls.Add(this.Button_Settings);
            this.Panel_Start.Controls.Add(this.Button_Save_Settings);
            this.Panel_Start.Controls.Add(this.Button_Start);
            this.Panel_Start.Location = new System.Drawing.Point(217, 436);
            this.Panel_Start.Name = "Panel_Start";
            this.Panel_Start.Size = new System.Drawing.Size(195, 100);
            this.Panel_Start.TabIndex = 5;
            // 
            // Button_Settings
            // 
            this.Button_Settings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Button_Settings.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Button_Settings.Location = new System.Drawing.Point(105, 46);
            this.Button_Settings.Name = "Button_Settings";
            this.Button_Settings.Size = new System.Drawing.Size(85, 49);
            this.Button_Settings.TabIndex = 2;
            this.Button_Settings.Text = "Go to settings";
            this.Button_Settings.UseVisualStyleBackColor = true;
            this.Button_Settings.Click += new System.EventHandler(this.Button_Settings_Click);
            // 
            // Button_Save_Settings
            // 
            this.Button_Save_Settings.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Button_Save_Settings.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Button_Save_Settings.Location = new System.Drawing.Point(3, 46);
            this.Button_Save_Settings.Name = "Button_Save_Settings";
            this.Button_Save_Settings.Size = new System.Drawing.Size(85, 49);
            this.Button_Save_Settings.TabIndex = 1;
            this.Button_Save_Settings.Text = "Save settings";
            this.Button_Save_Settings.UseVisualStyleBackColor = true;
            this.Button_Save_Settings.Click += new System.EventHandler(this.Button_Save_Settings_Click);
            // 
            // Button_Start
            // 
            this.Button_Start.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold);
            this.Button_Start.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Button_Start.Location = new System.Drawing.Point(3, 3);
            this.Button_Start.Name = "Button_Start";
            this.Button_Start.Size = new System.Drawing.Size(187, 37);
            this.Button_Start.TabIndex = 0;
            this.Button_Start.Text = "Start server";
            this.Button_Start.UseVisualStyleBackColor = true;
            this.Button_Start.Click += new System.EventHandler(this.Button_Start_Click);
            // 
            // Panel_Settings
            // 
            this.Panel_Settings.BackColor = System.Drawing.Color.Black;
            this.Panel_Settings.Controls.Add(this.Panel_Settings_1);
            this.Panel_Settings.Controls.Add(this.Panel_Settings_2);
            this.Panel_Settings.Location = new System.Drawing.Point(431, 118);
            this.Panel_Settings.Name = "Panel_Settings";
            this.Panel_Settings.Size = new System.Drawing.Size(400, 418);
            this.Panel_Settings.TabIndex = 7;
            this.Panel_Settings.Visible = false;
            // 
            // Panel_Settings_1
            // 
            this.Panel_Settings_1.BackColor = System.Drawing.Color.Red;
            this.Panel_Settings_1.Controls.Add(this.Settings_Button_Reset);
            this.Panel_Settings_1.Controls.Add(this.Settings_Combo_Time);
            this.Panel_Settings_1.Controls.Add(this.Settings_Label_Color);
            this.Panel_Settings_1.Controls.Add(this.Settings_Combo_Color);
            this.Panel_Settings_1.Controls.Add(this.Settings_Textbox_Backupnumber);
            this.Panel_Settings_1.Controls.Add(this.Settings_Label_Number);
            this.Panel_Settings_1.Controls.Add(this.Settings_Button_Back);
            this.Panel_Settings_1.Controls.Add(this.Settings_Label_Backup_1);
            this.Panel_Settings_1.Controls.Add(this.Settings_Input_Backuptime);
            this.Panel_Settings_1.Controls.Add(this.Settings_Check_Update);
            this.Panel_Settings_1.Controls.Add(this.Settings_Check_Backup);
            this.Panel_Settings_1.Controls.Add(this.Settings_Check_Executable);
            this.Panel_Settings_1.Controls.Add(this.Settings_Label_Title);
            this.Panel_Settings_1.Location = new System.Drawing.Point(0, 0);
            this.Panel_Settings_1.Name = "Panel_Settings_1";
            this.Panel_Settings_1.Size = new System.Drawing.Size(400, 147);
            this.Panel_Settings_1.TabIndex = 0;
            // 
            // Settings_Button_Reset
            // 
            this.Settings_Button_Reset.Location = new System.Drawing.Point(322, 33);
            this.Settings_Button_Reset.Name = "Settings_Button_Reset";
            this.Settings_Button_Reset.Size = new System.Drawing.Size(75, 23);
            this.Settings_Button_Reset.TabIndex = 20;
            this.Settings_Button_Reset.Text = "Reset ...";
            this.Settings_Button_Reset.UseVisualStyleBackColor = true;
            this.Settings_Button_Reset.Click += new System.EventHandler(this.Settings_Button_Reset_Click);
            // 
            // Settings_Combo_Time
            // 
            this.Settings_Combo_Time.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Settings_Combo_Time.FormattingEnabled = true;
            this.Settings_Combo_Time.Items.AddRange(new object[] {
            "hours",
            "minutes"});
            this.Settings_Combo_Time.Location = new System.Drawing.Point(167, 94);
            this.Settings_Combo_Time.Name = "Settings_Combo_Time";
            this.Settings_Combo_Time.Size = new System.Drawing.Size(80, 21);
            this.Settings_Combo_Time.Sorted = true;
            this.Settings_Combo_Time.TabIndex = 19;
            this.Settings_Combo_Time.TextChanged += new System.EventHandler(this.Settings_Combo_Time_TextChanged);
            // 
            // Settings_Label_Color
            // 
            this.Settings_Label_Color.AutoSize = true;
            this.Settings_Label_Color.BackColor = System.Drawing.Color.Transparent;
            this.Settings_Label_Color.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Settings_Label_Color.Location = new System.Drawing.Point(0, 70);
            this.Settings_Label_Color.Name = "Settings_Label_Color";
            this.Settings_Label_Color.Size = new System.Drawing.Size(130, 13);
            this.Settings_Label_Color.TabIndex = 1;
            this.Settings_Label_Color.Text = "Change background color";
            // 
            // Settings_Combo_Color
            // 
            this.Settings_Combo_Color.FormattingEnabled = true;
            this.Settings_Combo_Color.Items.AddRange(new object[] {
            "Blue",
            "Green",
            "Purple",
            "Red",
            "White",
            "Yellow"});
            this.Settings_Combo_Color.Location = new System.Drawing.Point(131, 67);
            this.Settings_Combo_Color.Name = "Settings_Combo_Color";
            this.Settings_Combo_Color.Size = new System.Drawing.Size(77, 21);
            this.Settings_Combo_Color.Sorted = true;
            this.Settings_Combo_Color.TabIndex = 2;
            this.Settings_Combo_Color.Text = "Default";
            this.Settings_Combo_Color.SelectedIndexChanged += new System.EventHandler(this.Settings_Combo_Color_SelectedIndexChanged);
            // 
            // Settings_Textbox_Backupnumber
            // 
            this.Settings_Textbox_Backupnumber.Location = new System.Drawing.Point(131, 117);
            this.Settings_Textbox_Backupnumber.MaxLength = 3;
            this.Settings_Textbox_Backupnumber.Name = "Settings_Textbox_Backupnumber";
            this.Settings_Textbox_Backupnumber.Size = new System.Drawing.Size(30, 20);
            this.Settings_Textbox_Backupnumber.TabIndex = 18;
            this.Settings_Textbox_Backupnumber.Text = "0";
            this.Settings_Textbox_Backupnumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Settings_Textbox_Backupnumber.TextChanged += new System.EventHandler(this.Settings_Textbox_Backupnumber_TextChanged);
            this.Settings_Textbox_Backupnumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Settings_Textbox_Backupnumber_KeyPress);
            // 
            // Settings_Label_Number
            // 
            this.Settings_Label_Number.AutoSize = true;
            this.Settings_Label_Number.BackColor = System.Drawing.Color.Transparent;
            this.Settings_Label_Number.Location = new System.Drawing.Point(3, 120);
            this.Settings_Label_Number.Name = "Settings_Label_Number";
            this.Settings_Label_Number.Size = new System.Drawing.Size(85, 13);
            this.Settings_Label_Number.TabIndex = 17;
            this.Settings_Label_Number.Text = "Backup number:";
            // 
            // Settings_Button_Back
            // 
            this.Settings_Button_Back.Location = new System.Drawing.Point(322, 4);
            this.Settings_Button_Back.Name = "Settings_Button_Back";
            this.Settings_Button_Back.Size = new System.Drawing.Size(75, 23);
            this.Settings_Button_Back.TabIndex = 15;
            this.Settings_Button_Back.Text = "< Back";
            this.Settings_Button_Back.UseVisualStyleBackColor = true;
            this.Settings_Button_Back.Click += new System.EventHandler(this.Settings_Button_Back_Click);
            // 
            // Settings_Label_Backup_1
            // 
            this.Settings_Label_Backup_1.AutoSize = true;
            this.Settings_Label_Backup_1.BackColor = System.Drawing.Color.Transparent;
            this.Settings_Label_Backup_1.Location = new System.Drawing.Point(3, 97);
            this.Settings_Label_Backup_1.Name = "Settings_Label_Backup_1";
            this.Settings_Label_Backup_1.Size = new System.Drawing.Size(122, 13);
            this.Settings_Label_Backup_1.TabIndex = 13;
            this.Settings_Label_Backup_1.Text = "Automatically backup in:";
            // 
            // Settings_Input_Backuptime
            // 
            this.Settings_Input_Backuptime.Location = new System.Drawing.Point(131, 94);
            this.Settings_Input_Backuptime.MaxLength = 3;
            this.Settings_Input_Backuptime.Name = "Settings_Input_Backuptime";
            this.Settings_Input_Backuptime.Size = new System.Drawing.Size(30, 20);
            this.Settings_Input_Backuptime.TabIndex = 12;
            this.Settings_Input_Backuptime.Text = "60";
            this.Settings_Input_Backuptime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Settings_Input_Backuptime.TextChanged += new System.EventHandler(this.Settings_Input_Backuptime_TextChanged);
            this.Settings_Input_Backuptime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Settings_Input_Backuptime_KeyPress);
            // 
            // Settings_Check_Update
            // 
            this.Settings_Check_Update.AutoSize = true;
            this.Settings_Check_Update.BackColor = System.Drawing.Color.Transparent;
            this.Settings_Check_Update.Location = new System.Drawing.Point(269, 120);
            this.Settings_Check_Update.Name = "Settings_Check_Update";
            this.Settings_Check_Update.Size = new System.Drawing.Size(128, 17);
            this.Settings_Check_Update.TabIndex = 11;
            this.Settings_Check_Update.Text = "Update automatically.";
            this.Settings_Check_Update.UseVisualStyleBackColor = false;
            this.Settings_Check_Update.Visible = false;
            // 
            // Settings_Check_Backup
            // 
            this.Settings_Check_Backup.AutoSize = true;
            this.Settings_Check_Backup.BackColor = System.Drawing.Color.Transparent;
            this.Settings_Check_Backup.Checked = true;
            this.Settings_Check_Backup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Settings_Check_Backup.Location = new System.Drawing.Point(6, 48);
            this.Settings_Check_Backup.Name = "Settings_Check_Backup";
            this.Settings_Check_Backup.Size = new System.Drawing.Size(130, 17);
            this.Settings_Check_Backup.TabIndex = 10;
            this.Settings_Check_Backup.Text = "Backup automatically.";
            this.Settings_Check_Backup.UseVisualStyleBackColor = false;
            this.Settings_Check_Backup.CheckedChanged += new System.EventHandler(this.Settings_Check_Backup_CheckedChanged);
            // 
            // Settings_Check_Executable
            // 
            this.Settings_Check_Executable.AutoSize = true;
            this.Settings_Check_Executable.BackColor = System.Drawing.Color.Transparent;
            this.Settings_Check_Executable.Checked = true;
            this.Settings_Check_Executable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Settings_Check_Executable.Enabled = false;
            this.Settings_Check_Executable.Location = new System.Drawing.Point(6, 25);
            this.Settings_Check_Executable.Name = "Settings_Check_Executable";
            this.Settings_Check_Executable.Size = new System.Drawing.Size(155, 17);
            this.Settings_Check_Executable.TabIndex = 8;
            this.Settings_Check_Executable.Text = "Need an executable to run.";
            this.Settings_Check_Executable.UseVisualStyleBackColor = false;
            this.Settings_Check_Executable.CheckedChanged += new System.EventHandler(this.Settings_Check_Executable_CheckedChanged);
            // 
            // Settings_Label_Title
            // 
            this.Settings_Label_Title.AutoSize = true;
            this.Settings_Label_Title.BackColor = System.Drawing.Color.Transparent;
            this.Settings_Label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Settings_Label_Title.Location = new System.Drawing.Point(3, 4);
            this.Settings_Label_Title.Name = "Settings_Label_Title";
            this.Settings_Label_Title.Size = new System.Drawing.Size(67, 17);
            this.Settings_Label_Title.TabIndex = 0;
            this.Settings_Label_Title.Text = "Settings";
            // 
            // Panel_Settings_2
            // 
            this.Panel_Settings_2.BackColor = System.Drawing.Color.Red;
            this.Panel_Settings_2.Controls.Add(this.Edit_Changes);
            this.Panel_Settings_2.Location = new System.Drawing.Point(0, 153);
            this.Panel_Settings_2.Name = "Panel_Settings_2";
            this.Panel_Settings_2.Size = new System.Drawing.Size(400, 265);
            this.Panel_Settings_2.TabIndex = 1;
            // 
            // Edit_Changes
            // 
            this.Edit_Changes.Location = new System.Drawing.Point(3, 3);
            this.Edit_Changes.Name = "Edit_Changes";
            this.Edit_Changes.ReadOnly = true;
            this.Edit_Changes.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.Edit_Changes.Size = new System.Drawing.Size(394, 259);
            this.Edit_Changes.TabIndex = 0;
            this.Edit_Changes.Text = "";
            // 
            // Panel_Running
            // 
            this.Panel_Running.BackColor = System.Drawing.Color.Red;
            this.Panel_Running.Controls.Add(this.Running_Button_Backupnow);
            this.Panel_Running.Controls.Add(this.Running_Button_Return);
            this.Panel_Running.Controls.Add(this.Running_Button_Stop);
            this.Panel_Running.Controls.Add(this.Running_Number_Local);
            this.Panel_Running.Controls.Add(this.Running_Number_Global);
            this.Panel_Running.Controls.Add(this.Running_Label_Time);
            this.Panel_Running.Controls.Add(this.Panel_Running_Ad);
            this.Panel_Running.Location = new System.Drawing.Point(861, 0);
            this.Panel_Running.Name = "Panel_Running";
            this.Panel_Running.Size = new System.Drawing.Size(250, 139);
            this.Panel_Running.TabIndex = 8;
            this.Panel_Running.Visible = false;
            // 
            // Running_Button_Backupnow
            // 
            this.Running_Button_Backupnow.Location = new System.Drawing.Point(87, 67);
            this.Running_Button_Backupnow.Name = "Running_Button_Backupnow";
            this.Running_Button_Backupnow.Size = new System.Drawing.Size(82, 23);
            this.Running_Button_Backupnow.TabIndex = 6;
            this.Running_Button_Backupnow.Text = "Backup now";
            this.Running_Button_Backupnow.UseVisualStyleBackColor = true;
            this.Running_Button_Backupnow.Click += new System.EventHandler(this.Running_Button_Backupnow_Click);
            // 
            // Running_Button_Return
            // 
            this.Running_Button_Return.Location = new System.Drawing.Point(6, 67);
            this.Running_Button_Return.Name = "Running_Button_Return";
            this.Running_Button_Return.Size = new System.Drawing.Size(75, 23);
            this.Running_Button_Return.TabIndex = 5;
            this.Running_Button_Return.Text = "< Back";
            this.Running_Button_Return.UseVisualStyleBackColor = true;
            this.Running_Button_Return.Click += new System.EventHandler(this.Running_Button_Return_Click);
            // 
            // Running_Button_Stop
            // 
            this.Running_Button_Stop.Location = new System.Drawing.Point(172, 67);
            this.Running_Button_Stop.Name = "Running_Button_Stop";
            this.Running_Button_Stop.Size = new System.Drawing.Size(75, 23);
            this.Running_Button_Stop.TabIndex = 4;
            this.Running_Button_Stop.Text = "Stop";
            this.Running_Button_Stop.UseVisualStyleBackColor = true;
            this.Running_Button_Stop.Click += new System.EventHandler(this.Running_Button_Stop_Click);
            // 
            // Running_Number_Local
            // 
            this.Running_Number_Local.AutoSize = true;
            this.Running_Number_Local.Location = new System.Drawing.Point(3, 35);
            this.Running_Number_Local.Name = "Running_Number_Local";
            this.Running_Number_Local.Size = new System.Drawing.Size(166, 13);
            this.Running_Number_Local.TabIndex = 3;
            this.Running_Number_Local.Text = "Backup number on this session: 0";
            // 
            // Running_Number_Global
            // 
            this.Running_Number_Global.AutoSize = true;
            this.Running_Number_Global.Location = new System.Drawing.Point(3, 22);
            this.Running_Number_Global.Name = "Running_Number_Global";
            this.Running_Number_Global.Size = new System.Drawing.Size(128, 13);
            this.Running_Number_Global.TabIndex = 2;
            this.Running_Number_Global.Text = "Backup number overall: 0";
            // 
            // Running_Label_Time
            // 
            this.Running_Label_Time.AutoSize = true;
            this.Running_Label_Time.Location = new System.Drawing.Point(3, 9);
            this.Running_Label_Time.Name = "Running_Label_Time";
            this.Running_Label_Time.Size = new System.Drawing.Size(136, 13);
            this.Running_Label_Time.TabIndex = 1;
            this.Running_Label_Time.Text = "Next backup in 60 minutes.";
            // 
            // Panel_Running_Ad
            // 
            this.Panel_Running_Ad.BackColor = System.Drawing.Color.Maroon;
            this.Panel_Running_Ad.Controls.Add(this.Running_Label_Ad);
            this.Panel_Running_Ad.Location = new System.Drawing.Point(0, 96);
            this.Panel_Running_Ad.Name = "Panel_Running_Ad";
            this.Panel_Running_Ad.Size = new System.Drawing.Size(250, 40);
            this.Panel_Running_Ad.TabIndex = 0;
            // 
            // Running_Label_Ad
            // 
            this.Running_Label_Ad.AutoSize = true;
            this.Running_Label_Ad.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Running_Label_Ad.LinkColor = System.Drawing.Color.Black;
            this.Running_Label_Ad.Location = new System.Drawing.Point(3, 0);
            this.Running_Label_Ad.Name = "Running_Label_Ad";
            this.Running_Label_Ad.Size = new System.Drawing.Size(240, 31);
            this.Running_Label_Ad.TabIndex = 0;
            this.Running_Label_Ad.TabStop = true;
            this.Running_Label_Ad.Text = "Want to advertise?";
            this.Running_Label_Ad.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Running_Label_Ad_LinkClicked);
            // 
            // Panel_Ad
            // 
            this.Panel_Ad.BackColor = System.Drawing.Color.Red;
            this.Panel_Ad.BackgroundImage = global::Minecraft_Server_Auto_Backup.Properties.Resources.Bottom_Banner_14_2_2012;
            this.Panel_Ad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Panel_Ad.Location = new System.Drawing.Point(12, 542);
            this.Panel_Ad.Name = "Panel_Ad";
            this.Panel_Ad.Size = new System.Drawing.Size(400, 100);
            this.Panel_Ad.TabIndex = 6;
            this.Panel_Ad.Click += new System.EventHandler(this.Panel_Ad_Click);
            // 
            // Panel_Title
            // 
            this.Panel_Title.BackColor = System.Drawing.Color.Red;
            this.Panel_Title.BackgroundImage = global::Minecraft_Server_Auto_Backup.Properties.Resources.Title;
            this.Panel_Title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Panel_Title.Location = new System.Drawing.Point(12, 12);
            this.Panel_Title.Name = "Panel_Title";
            this.Panel_Title.Size = new System.Drawing.Size(400, 100);
            this.Panel_Title.TabIndex = 0;
            // 
            // Panel_Updating
            // 
            this.Panel_Updating.BackColor = System.Drawing.Color.White;
            this.Panel_Updating.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Panel_Updating.Controls.Add(this.Updating_Image);
            this.Panel_Updating.Controls.Add(this.Updating_Label);
            this.Panel_Updating.Location = new System.Drawing.Point(861, 224);
            this.Panel_Updating.Name = "Panel_Updating";
            this.Panel_Updating.Size = new System.Drawing.Size(250, 144);
            this.Panel_Updating.TabIndex = 9;
            this.Panel_Updating.Visible = false;
            // 
            // Updating_Image
            // 
            this.Updating_Image.Image = global::Minecraft_Server_Auto_Backup.Properties.Resources.ajax_loader;
            this.Updating_Image.Location = new System.Drawing.Point(10, 106);
            this.Updating_Image.Name = "Updating_Image";
            this.Updating_Image.Size = new System.Drawing.Size(220, 19);
            this.Updating_Image.TabIndex = 1;
            this.Updating_Image.TabStop = false;
            // 
            // Updating_Label
            // 
            this.Updating_Label.AutoSize = true;
            this.Updating_Label.BackColor = System.Drawing.Color.White;
            this.Updating_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Updating_Label.ForeColor = System.Drawing.Color.Black;
            this.Updating_Label.Location = new System.Drawing.Point(5, 24);
            this.Updating_Label.Name = "Updating_Label";
            this.Updating_Label.Size = new System.Drawing.Size(242, 52);
            this.Updating_Label.TabIndex = 0;
            this.Updating_Label.Text = "Checking for an update.\r\nPlease wait.";
            this.Updating_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ping_Version
            // 
            this.Ping_Version.WorkerSupportsCancellation = true;
            this.Ping_Version.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Ping_Version_DoWork);
            this.Ping_Version.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Ping_Version_RunWorkerCompleted);
            // 
            // New_GUI
            // 
            this.New_GUI.Location = new System.Drawing.Point(115, 43);
            this.New_GUI.Name = "New_GUI";
            this.New_GUI.Size = new System.Drawing.Size(75, 23);
            this.New_GUI.TabIndex = 4;
            this.New_GUI.Text = "New GUI";
            this.New_GUI.UseVisualStyleBackColor = true;
            this.New_GUI.Click += new System.EventHandler(this.New_GUI_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1139, 654);
            this.Controls.Add(this.Panel_Updating);
            this.Controls.Add(this.Panel_Running);
            this.Controls.Add(this.Panel_Settings);
            this.Controls.Add(this.Panel_Ad);
            this.Controls.Add(this.Panel_Start);
            this.Controls.Add(this.Panel_Version);
            this.Controls.Add(this.Panel_Backup_Folder);
            this.Controls.Add(this.Panel_Server_Folder);
            this.Controls.Add(this.Panel_File);
            this.Controls.Add(this.Panel_Title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Minecraft Server Auto Backup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Panel_File.ResumeLayout(false);
            this.Panel_File.PerformLayout();
            this.Panel_Server_Folder.ResumeLayout(false);
            this.Panel_Server_Folder.PerformLayout();
            this.Panel_Backup_Folder.ResumeLayout(false);
            this.Panel_Backup_Folder.PerformLayout();
            this.Panel_Version.ResumeLayout(false);
            this.Panel_Version.PerformLayout();
            this.Panel_Start.ResumeLayout(false);
            this.Panel_Settings.ResumeLayout(false);
            this.Panel_Settings_1.ResumeLayout(false);
            this.Panel_Settings_1.PerformLayout();
            this.Panel_Settings_2.ResumeLayout(false);
            this.Panel_Running.ResumeLayout(false);
            this.Panel_Running.PerformLayout();
            this.Panel_Running_Ad.ResumeLayout(false);
            this.Panel_Running_Ad.PerformLayout();
            this.Panel_Updating.ResumeLayout(false);
            this.Panel_Updating.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Updating_Image)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Panel_Title;
        private System.Windows.Forms.Panel Panel_File;
        private System.Windows.Forms.Panel Panel_Server_Folder;
        private System.Windows.Forms.Panel Panel_Backup_Folder;
        private System.Windows.Forms.Panel Panel_Version;
        private System.Windows.Forms.Panel Panel_Start;
        private System.Windows.Forms.Panel Panel_Ad;
        private System.Windows.Forms.TextBox Textbox_Path_File;
        private System.Windows.Forms.Label Lbl_Path_File;
        private System.Windows.Forms.Button Button_Path_File_Select;
        private System.Windows.Forms.Button Button_Path_Folder_Select;
        private System.Windows.Forms.TextBox Textbox_Path_Folder;
        private System.Windows.Forms.Label Lbl_Path_Folder;
        private System.Windows.Forms.Button Button_Path_Backup_Selection;
        private System.Windows.Forms.TextBox Textbox_Path_Backup;
        private System.Windows.Forms.Label Lbl_Path_Backup;
        private System.Windows.Forms.Button Button_Update_Check;
        private System.Windows.Forms.Button Button_Help;
        private System.Windows.Forms.Button Button_Settings;
        private System.Windows.Forms.Button Button_Save_Settings;
        private System.Windows.Forms.Button Button_Start;
        private System.Windows.Forms.Panel Panel_Settings;
        private System.Windows.Forms.Panel Panel_Settings_1;
        private System.Windows.Forms.Panel Panel_Settings_2;
        private System.Windows.Forms.CheckBox Settings_Check_Executable;
        private System.Windows.Forms.Label Settings_Label_Title;
        private System.Windows.Forms.RichTextBox Edit_Changes;
        private System.Windows.Forms.CheckBox Settings_Check_Backup;
        private System.Windows.Forms.Button Settings_Button_Back;
        private System.Windows.Forms.Label Settings_Label_Backup_1;
        private System.Windows.Forms.TextBox Settings_Input_Backuptime;
        private System.Windows.Forms.Panel Panel_Running;
        private System.Windows.Forms.Panel Panel_Running_Ad;
        private System.Windows.Forms.Button Running_Button_Backupnow;
        private System.Windows.Forms.Button Running_Button_Return;
        private System.Windows.Forms.Button Running_Button_Stop;
        private System.Windows.Forms.Label Running_Number_Local;
        private System.Windows.Forms.Label Running_Number_Global;
        private System.Windows.Forms.Label Running_Label_Time;
        private System.Windows.Forms.LinkLabel Running_Label_Ad;
        private System.Windows.Forms.TextBox Settings_Textbox_Backupnumber;
        private System.Windows.Forms.Label Settings_Label_Number;
        private System.Windows.Forms.Button Button_Clear_File;
        private System.Windows.Forms.Button Button_Clear_Folder;
        private System.Windows.Forms.Button Button_Clear_Backup;
        private System.Windows.Forms.Panel Panel_Updating;
        private System.Windows.Forms.Label Updating_Label;
        private System.Windows.Forms.PictureBox Updating_Image;
        private System.Windows.Forms.CheckBox Settings_Check_Update;
        private System.Windows.Forms.ComboBox Settings_Combo_Time;
        public System.Windows.Forms.TextBox Textbox_Update;
        private System.ComponentModel.BackgroundWorker Ping_Version;
        private System.ComponentModel.BackgroundWorker Download_Updater;
        private System.Windows.Forms.ComboBox Settings_Combo_Color;
        private System.Windows.Forms.Label Settings_Label_Color;
        private System.Windows.Forms.Button Settings_Button_Reset;
        private System.Windows.Forms.Button New_GUI;


    }
}

