﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Minecraft_Server_Auto_Backup
{
    public partial class Help : Form
    {

        // Initialize strings for backgroundworker
        public string Contact_Mail;
        public string Contact_Name;
        public string Contact_Type;
        public string Contact_Message;

        public Help()
        {
            // Initialize components
            InitializeComponent();

            // Set GUI size
            this.Size                   = new Size(490, 490);

            // Set panel coordinates
            Panel_Contact.Location      = new Point(143, 41);
            Panel_Subjects.Location     = new Point(143, 41);

            // Initialize contact form
            Contact_Combo_Type.Text     = "Contact";
            Contact_Textbox_Name.Text   = Properties.Settings.Default.Contact_Name;
            Contact_Textbox_Email.Text  = Properties.Settings.Default.Contact_Mail;
        }

        #region Button actions

            // Button Exit
            private void Main_Button_Exit_Click(object sender, EventArgs e)
            {
                // Exit help screen
                this.Hide();
            }

            // Button Home
            private void Main_Button_Home_Click(object sender, EventArgs e)
            {
                // Set GUI changes
                Panel_Home.Visible      = true;
                Panel_Subjects.Visible  = false;
                Panel_Contact.Visible   = false;
            }

            // Button Contact
            private void Main_Button_Contact_Click(object sender, EventArgs e)
            {
                // Set GUI changes
                Panel_Contact.Visible   = true;
                Panel_Subjects.Visible  = false;
                Panel_Home.Visible      = false;
            }

            // Contact form - send message
            private void Contact_Button_Send_Click(object sender, EventArgs e)
            {
                // Check inputboxes
                if (Contact_Textbox_Name.Text == "" | Contact_Textbox_Email.Text == "" | Contact_Textbox_Message.Text == "" | Contact_Combo_Type.Text == "")
                {
                    MessageBox.Show("You forgot an inputbox, please fill in every thing!");
                    return;
                }

                // Set GUI changes
                Contact_Button_Clear.Enabled    = false;
                Contact_Button_Send.Enabled     = false;
                Contact_Combo_Type.Enabled      = false;
                Contact_Textbox_Email.Enabled   = false;
                Contact_Textbox_Message.Enabled = false;
                Contact_Textbox_Name.Enabled    = false;
                Contact_Image_Sending.Visible   = true;

                // Set strings
                Contact_Name    = Contact_Textbox_Name.Text;
                Contact_Mail    = Contact_Textbox_Email.Text;
                Contact_Message = Contact_Textbox_Message.Text;

                // Activate background proces
                BG_Sendmessage.RunWorkerAsync();
            }

            // Contact form - clear fields
            private void Contact_Button_Clear_Click(object sender, EventArgs e)
            {
                // GUI changes
                Contact_Textbox_Email.Text      = "";
                Contact_Textbox_Message.Text    = "";
                Contact_Textbox_Name.Text       = "";
                Contact_Combo_Type.Text         = "Contact";

                // Write to settings
                Properties.Settings.Default.Contact_Name = "";
                Properties.Settings.Default.Contact_Mail = "";
            }

        #endregion

        #region Subjects region

        // When you select a subject
        private void Main_Listbox_Subjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set component visibility
            Panel_Subjects.Visible = true;
            Panel_Home.Visible = false;
            Panel_Contact.Visible = false;

            // Clear the textbox
            Subjects_Textbox.Text = "";

            // Execute task
            Subjectbox_Settext(Main_Listbox_Subjects.SelectedItem.ToString());
        }

        // Task to set text
        private void Subjectbox_Settext(string subject)
        {
            if (subject == "About")
            {
                Subjects_Textbox.Text = 
                    "Program name: " + Properties.Resources.Program_Name + 
                    "\r\nProgram author: " + Properties.Resources.Program_Author + 
                    "\r\nCopyright: " + Properties.Resources.Program_Copyright + 
                    "\r\n\r\n\r\n" + 
                    "You may not copy or illegaly distribute this program.";
            }
            else if (subject == "Backup")
            {
                Subjects_Textbox.Text = 
                    "This section will explain to you how the backup and restore process works." + 
                    "\r\n\r\n" + 
                    "Please select a section on the left side for more information.";
            }
            else if (subject == "Backup - make backup")
            {
                Subjects_Textbox.Text = 
                    "";
            }
            else if (subject == "Backup - restore")
            {
                Subjects_Textbox.Text = 
                    "";
            }
            else if (subject == "Update")
            {
                Subjects_Textbox.Text = 
                    "The program is able to automatically check for updates when the program starts. " +
                    "You can disable this option in the 'Settings'-screen. Letting this option enabled is recommended." +
                    "\r\n\r\n" + 
                    "When the program has found an update, the program will ask you to update the program. " + 
                    "The program won't update itself automatically. If you agreed to update, the program will download " + 
                    "the updater/installer and will help you update itself. It's very easy. Just let the program do it's job!" + 
                    "\r\n\r\n" + 
                    "Thats all you have to know for now.";
            }
            else if (subject == "Settings")
            {
                Subjects_Textbox.Text = 
                    "Inside this panel you can change some options." + 
                    "\r\n\r\n" + 
                    "There are not many options, but it's important to check the page, " +
                    "because of some important options, like options for the backup proces." + 
                    "\r\n\r\n" + 
                    "You dont have to press any save buttons, if you change an option, it saves itself!";
            }
        }

        #endregion

        #region Background workers

            // BG worker strings
            public bool Error = false;
            public string Contact_Exception;
            
            private void BG_Sendmessage_DoWork(object sender, DoWorkEventArgs e)
            {
                // Initialize mail
                System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage();

                // Set-up mail
                Message.To.Add("lefersyouri@gmail.com");
                Message.From        = new System.Net.Mail.MailAddress(Contact_Mail);
                Message.Subject     = "Automatic Backup - " + Contact_Type + " - from: " + Contact_Name;
                Message.Body        = 
                    "Message information\r\n" + 
                    "Name: "            + Contact_Name + "\r\n" + 
                    "Mail adress: "     + Contact_Mail + "\r\n" + 
                    "Contact type: "    + Contact_Type + "\r\n" + 
                    "Date: "            + DateTime.Now + "\r\n" + 
                    "\r\n" + 
                    "Message:\r\n' "    + Contact_Message + " '\r\n" + 
                    "\r\n" + 
                    "End of the contact mail.";

                // Initialize SMTP client
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

                // Set-up smtp client
                smtp.UseDefaultCredentials  = false;
                smtp.Credentials            = new System.Net.NetworkCredential("lefersyouri@gmail.com", "fctwente65");
                smtp.EnableSsl              = true;
                smtp.Port                   = 587;
                smtp.Host                   = "smtp.gmail.com";

                // Send the message
                try
                {
                    smtp.Send(Message);
                }
                catch (Exception f)
                {
                    Error = true;
                    Contact_Exception = f.Message;
                }
            }

            private void BG_Sendmessage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            {
                // Error occured
                if (Error)
                {
                    MessageBox.Show("An error occured while sending the message. Please try again later. Error message: " + Contact_Exception);
                }
                // No error occured
                else if (!Error)
                {
                    // Show message
                    MessageBox.Show("The message has been sent succesfully. Thank you for posting!");

                    // Empty message-textbox
                    Contact_Textbox_Message.Text                = "";
                    Contact_Combo_Type.Text                     = "Contact";

                    // Save mail and name
                    Properties.Settings.Default.Contact_Name    = Contact_Textbox_Name.Text;
                    Properties.Settings.Default.Contact_Mail    = Contact_Textbox_Email.Text;
                }

                // GUI changes
                Contact_Button_Clear.Enabled                    = true;
                Contact_Button_Send.Enabled                     = true;
                Contact_Combo_Type.Enabled                      = true;
                Contact_Textbox_Email.Enabled                   = true;
                Contact_Textbox_Message.Enabled                 = true;
                Contact_Textbox_Name.Enabled                    = true;
                Contact_Image_Sending.Visible                   = false;
            }

        #endregion
    }
}
