﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Update;

namespace Minecraft_Server_Auto_Backup
{
    public partial class Update_Form : Form
    {
        Update.Update Update_File = new Update.Update();

        public Update_Form()
        {
            InitializeComponent();

            if (Properties.Settings.Default.Settings_Updatechannel == "Stable")
            { Button_Stable.Checked = true; }
            else { Button_Beta.Checked = true; }

            Back_Update_Check.RunWorkerAsync();
        }

        private void Button_Exit_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Update_Form_FormClosing(object sender, FormClosingEventArgs e)
        {

            this.Hide();

        }

        private void Button_Update_Click(object sender, EventArgs e)
        {

            GUI(0);
            Back_Update.RunWorkerAsync();

        }

        private void Button_Update2_Click(object sender, EventArgs e)
        {

            GUI(0);
            Back_Update.RunWorkerAsync();

        }

        private void Button_Reload_Click(object sender, EventArgs e)
        {

            GUI(0);
            Back_Update_Check.RunWorkerAsync();

        }

        private void Button_Stable_Click(object sender, EventArgs e)
        {

            
            Button_Stable.Checked = true;
            Button_Beta.Checked = false;

            Properties.Settings.Default.Settings_Updatechannel = "Stable";

            GUI(0);
            Update_Finished();

        }

        private void Button_Beta_Click(object sender, EventArgs e)
        {

            Button_Stable.Checked = false;
            Button_Beta.Checked = true;

            Properties.Settings.Default.Settings_Updatechannel = "Beta";

            GUI(0);
            Update_Finished();

        }

        private void Button_About_Click(object sender, EventArgs e)
        {
            AboutBox1 aboutbox = new AboutBox1();
            aboutbox.Show();
        }

        private void Button_Help_Click(object sender, EventArgs e)
        {

        }

        /// <summary Tasks>

        /// Region: Tasks

        /// </summary>

        private void GUI(int State)
        {

            if (State == 0)
            {

                Button_Cancel.Enabled = false;
                Button_Reload.Enabled = false;
                Button_Update.Enabled = false;
                Button_Update2.Enabled = false;

                Panel_Loading.Visible = true;

            }
            else if (State == 1)
            {

                Button_Cancel.Enabled = true;
                Button_Reload.Enabled = true;
                Button_Update.Enabled = true;
                Button_Update2.Enabled = true;

                Panel_Loading.Visible = false;

            }
            else if (State == 2)
            {

                Button_Cancel.Enabled = true;
                Button_Reload.Enabled = true;
                Button_Update.Enabled = false;
                Button_Update2.Enabled = false;

                Panel_Loading.Visible = false;

            }

        }

        private void Update_Finished()
        {

            GUI(0);
            Back_Update_Check.RunWorkerAsync();

        }

        /// <summary>
        
        /// Region: Backgroundworkers

        /// </summary>

        string Version_Latest;
        string Release_Notes;
        string Updating;

        private void Back_Update_Check_DoWork(object sender, DoWorkEventArgs e)
        {

            try
            {

                Version_Latest = Update_File.Update_Check("http://www.automaticbackup.nl/" + Properties.Settings.Default.Settings_Updatechannel + "/version");
                Release_Notes = Update_File.Update_Releasenotes("http://www.automaticbackup.nl/" + Properties.Settings.Default.Settings_Updatechannel + "/releasenotes");
            
            }
            catch (Exception)
            {

                Version_Latest = "Error";
                Release_Notes = "Error";

            }

            if (Version_Latest == "Error")
            {

                try
                {

                    Version_Latest = Update_File.Update_Check("http://msab.yourilefers.nl/" + Properties.Settings.Default.Settings_Updatechannel + "/version");
                    Release_Notes = Update_File.Update_Releasenotes("http://msab.yourilefers.nl/" + Properties.Settings.Default.Settings_Updatechannel + "/releasenotes");

                }
                catch (Exception)
                {

                    Version_Latest = "Error";
                    Release_Notes = "Error";

                }

            }
        }

        private void Back_Update_Check_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (Version_Latest != "Error" | Version_Latest != "" | Version_Latest != "Done")
            {

                Label_Info.Text = "Current version: " + Properties.Resources.Version_Main + "\r\nLatest version: " + Version_Latest;
                Release_Notes = Release_Notes.Replace("<br />", "/r/n");
                Textbox_Releasenotes.Text = Release_Notes;

                if (Version_Latest != Properties.Resources.Version_Main)
                {

                    Label_Update.Text = "Do you want to update?";
                    GUI(1);

                }
                else
                {

                    Label_Update.Text = "There is no update available.";
                    GUI(2);

                }

            }
            else
            {

                Label_Info.Text = "Current version: " + Properties.Resources.Version_Main + "\r\nLatest version: " + Version_Latest;

                Label_Update.Text = "Error occured! Please retry.";
                GUI(2);

            }
        }

        string except;

        private void Back_Update_DoWork(object sender, DoWorkEventArgs e)
        {

            try {

                Updating = Update_File.Start_Update("http://www.automaticbackup.nl/" + Properties.Settings.Default.Settings_Updatechannel + "/Info.ini", Properties.Settings.Default.Settings_Updatechannel);
            
            }
            catch (Exception z) {

                Updating = "Error";
                except = Convert.ToString(z);

            }

            if (Updating == "Error") {

                try {

                    Updating = Update_File.Start_Update("http://msab.yourilefers.nl/" + Properties.Settings.Default.Settings_Updatechannel + "/Info.ini", Properties.Settings.Default.Settings_Updatechannel);
                
                }
                catch (Exception z) {

                    Updating = "Error";
                    except = Convert.ToString(z);

                }

            }

        }

        private void Back_Update_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (Updating == "Error" | Updating == "Done")
            {

                MessageBox.Show("An error has occured. The program will re-check for a possible update. Reason: " + except);
                Update_Finished();

            }
            else if (Updating == "Finished_Close")
            {

                Application.Exit();

            }
            else if (Updating == "Finished")
            {

                MessageBox.Show("The update has finished. Rechecking for an update.");
                Update_Finished();

            }

        }

    }
}
